/*
CREATE TABLE ST_ACCOUNT_TB(
VIEW_CNT		INT			NOT NULL	IDENTITY(10001,1),
ADD_DT			DATETIME	NOT NULL	DEFAULT GETDATE(),
USER_ID			VARCHAR(20)	NOT NULL	PRIMARY KEY,
USER_PW			VARCHAR(60),
ISSTUDENT_FL	BIT			NOT NULL,
EMAIL_NM		VARCHAR(40)	NOT NULL
)

CREATE TABLE ST_STUDYLOG_TB(
VIEW_CNT	INT				NOT NULL	PRIMARY KEY		IDENTITY(10001,1),
USER_ID		VARCHAR(20),
SBJNAME_NM	NVARCHAR(30),
START_DT	DATETIME		NOT NULL,
FINISH_DT	DATETIME		NOT NULL,
TOTAL_DT	DATETIME		NOT NULL,
CONCENT_DT	DATETIME		NOT NULL,
CONCENT_NO	FLOAT			NOT NULL
)

CREATE TABLE ST_REMOTE_TB(
VIEW_CNT	INT			NOT NULL	IDENTITY(10001,1),
ADD_DT		DATETIME	NOT NULL	DEFAULT GETDATE(),
USER_ID		VARCHAR(20)	NOT NULL	PRIMARY KEY,
PARENT_ID	VARCHAR(20)	NOT NULL
)

CREATE TABLE ST_AUTHNUM_TB(
VIEW_CNT	INT			NOT NULL	IDENTITY(10001,1),
ADD_DT		DATETIME	NOT NULL	DEFAULT	GETDATE(),
USER_ID		VARCHAR(20)	NOT NULL	PRIMARY KEY,
AUTH_NO		VARCHAR(6)	NOT NULL
)

CREATE TABLE ST_STUDYPROCESS_TB(
VIEW_CNT	INT				NOT NULL	IDENTITY(10001,1),
USER_ID		VARCHAR(20)		NOT NULL	PRIMARY KEY,
SBJNAME_NM	NVARCHAR(30)	NOT NULL,
START_DT	DATETIME		NOT NULL,
TOTAL_DT	DATETIME		NOT NULL,
IMGNAME_NM	VARCHAR(40)		NOT NULL
)
*/

/*
DECLARE @passwd VARCHAR(60)
DECLARE @crypted_passwd VARCHAR(60)
set @passwd='pass1'
select @crypted_passwd = dbo.Bcrypt('pass1',11)
print @crypted_passwd
-- Check if the passwords match
select dbo.CheckPassword(@passwd,@crypted_passwd)
select dbo.CheckPassword('pass1',(select userPW from userAccount where userID='test1')) as result
*/


USE StudyTracker
