# Study Tracker Project

## It helps students to study.

### Features

- 오늘의 공부 목표 설정
- 스마트 방석과 얼굴 인식을 통한 공부 시간 기록 기능
- 전체 시간 : 공부 시작부터 종료까지의 전체 시간을 기록
- 얼마나 집중했나요? : 얼굴이 인식되고 방석에 올바르게 앉아있을 때의 시간을 기록
- 공부 시간 및 집중도는 서버에 연동되어 통합적으로 관리
- 부모님의 자녀 확인 기능


### It runs with..

- Android 5.0 Lolipop or later (Supported 6.0 permission but it need to be fixed)
- Windows 10 Home IIS + Apache Tomcat Web Server to run JSP
- MS-SQL 2014
- Study Cushion : https://gitlab.com/racersyun/StudyCushion


### Example Image
<img src="/uploads/1c598013ee82cf17b495e0f0b4b79ae8/2-1.png"  width="700">

### References

- OpenCV Haar-Cascade XML Data : https://github.com/opencv/opencv/tree/master/data/haarcascades
- BCrypt for encrypt user passwords
