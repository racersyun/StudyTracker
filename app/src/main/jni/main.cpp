#include <jni.h>
#include <comracersyun_naver_blog_studytracker_CameraServiceClass.h>
#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/objdetect/objdetect.hpp>

#include <stdio.h>
#include <iostream>

using namespace std;
using namespace cv;

CascadeClassifier face_cascade;
CascadeClassifier eyes_cascade;

void histogramEqualization(Mat &frame);

int detectAndDisplay(Mat &frame);

JNIEXPORT jint

JNICALL Java_com_racersyun_studytracker_CameraServiceClass_EyeTracking
        (JNIEnv *env, jobject thiz, jint width, jint height, jlong matAddrInput,
         jlong matAddrOutput, jstring facePath, jstring eyePath) {

    Mat &input = *(Mat *) matAddrInput;
    Mat &output = *(Mat *) matAddrOutput;

    jboolean isSucceed;

    String face_cascade_name = (env)->GetStringUTFChars(facePath, &isSucceed);
    String eyes_cascade_name = (env)->GetStringUTFChars(eyePath, &isSucceed);

    if (!face_cascade.load(face_cascade_name) || !eyes_cascade.load(eyes_cascade_name)) {
        return -1;
    }

    else {
        int detectResult = 0;

        //histogramEqualization(input);
        detectResult = detectAndDisplay(input);
        output = input;

        return detectResult;
    }
}

void histogramEqualization(Mat &frame) {
    if (frame.channels() >= 3) {
        Mat yCrCbImg;
        cvtColor(frame, yCrCbImg, COLOR_RGB2YCrCb);

        vector <Mat> channels;
        split(yCrCbImg, channels);

        equalizeHist(channels[0], channels[0]);

        Mat resultImg;
        merge(channels, resultImg);

        cvtColor(resultImg, resultImg, COLOR_YCrCb2RGB);

        frame = resultImg;
    }
}

int detectAndDisplay(Mat &frame) {
    vector <Rect> faces;
    Mat frame_gray;
    int detectResult = 0;

    cvtColor(frame, frame_gray, COLOR_RGBA2GRAY);
    equalizeHist(frame_gray, frame_gray);

    face_cascade.detectMultiScale(frame_gray, faces, 1.1, 2, 0 | CASCADE_SCALE_IMAGE, Size(30, 30));

    for (size_t i = 0; i < faces.size(); i++) {
        detectResult = 1;
        Point center(faces[i].x + faces[i].width / 2, faces[i].y + faces[i].height / 2);
        ellipse(frame, center, Size(faces[i].width / 2, faces[i].height / 2), 0, 0, 360,
                Scalar(255, 0, 255), 4, 8, 0);

        Mat faceROI = frame_gray(faces[i]);
        vector <Rect> eyes;

        eyes_cascade.detectMultiScale(faceROI, eyes, 1.1, 2, 0 | CASCADE_SCALE_IMAGE, Size(30, 30));

        for (size_t j = 0; j < eyes.size(); j++) {
            detectResult = 2;
            Point eye_center(faces[i].x + eyes[j].x + eyes[j].width / 2,
                             faces[i].y + eyes[j].y + eyes[j].height / 2);
            int radius = cvRound((eyes[j].width + eyes[j].height) * 0.25);
            circle(frame, eye_center, radius, Scalar(255, 0, 0), 4, 8, 0);
        }
    }
    return detectResult;
}