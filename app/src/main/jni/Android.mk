LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

OPENCVROOT:=${LOCAL_PATH}/../../../../OpenCV4.5.4
OPENCV_CAMERA_MODULES:=on
OPENCV_INSTALL_MODULES:=on
OPENCV_LIB_TYPE:=SHARED

include ${OPENCVROOT}/native/jni/OpenCV.mk

LOCAL_SRC_FILES := main.cpp
LOCAL_LDLIBS += -llog
LOCAL_MODULE := eye-tracking

include $(BUILD_SHARED_LIBRARY)