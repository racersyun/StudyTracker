/* DO NOT EDIT THIS FILE - it is machine generated */
#include <jni.h>
/* Header for class com_racersyun_studytracker_CameraServiceClass */

#ifndef _Included_com_racersyun_studytracker_CameraServiceClass
#define _Included_com_racersyun_studytracker_CameraServiceClass
#ifdef __cplusplus
extern "C" {
#endif
#undef com_racersyun_studytracker_CameraServiceClass_CSC_MODE_MAIN
#define com_racersyun_studytracker_CameraServiceClass_CSC_MODE_MAIN 1L
#undef com_racersyun_studytracker_CameraServiceClass_CSC_MODE_PROCESS
#define com_racersyun_studytracker_CameraServiceClass_CSC_MODE_PROCESS 2L
#undef com_racersyun_studytracker_CameraServiceClass_CSC_MODE_SETTING
#define com_racersyun_studytracker_CameraServiceClass_CSC_MODE_SETTING 3L
/*
 * Class:     com_racersyun_studytracker_CameraServiceClass
 * Method:    EyeTracking
 * Signature: (IIJJ)I
 */
JNIEXPORT jint JNICALL Java_com_racersyun_studytracker_CameraServiceClass_EyeTracking
        (JNIEnv * , jobject, jint, jint, jlong, jlong, jstring, jstring);

#ifdef __cplusplus
}
#endif
#endif
