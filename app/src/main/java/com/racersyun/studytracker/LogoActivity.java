package com.racersyun.studytracker;

import android.content.Intent;
import android.content.res.AssetManager;
import android.os.Bundle;
import android.os.Handler;

import androidx.appcompat.app.AppCompatActivity;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.InputStream;

/**
 * Created by racer on 2016-07-29.
 * XML 파일 로컬 저장 후 메인으로 이동
 */
public class LogoActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_logo);

        new Handler().postDelayed(() -> {
            if (!isExistFaceDetectXml()) {
                getFaceDetectXml();
            }
            moveMainActivity();
        }, 1000);
    }

    /**
     * 얼굴인식에 필요한 XML 파일이 내부저장소에 있는지 확인
     * 없을 경우 assets의 파일 복사 후 진행 필요
     */
    private boolean isExistFaceDetectXml() {
        final String faceXmlFileName = "haarcascade_frontalface_alt.xml";
        final String eyeXmlFileName = "haarcascade_eye_tree_eyeglasses.xml";
        final String faceXmlFilePath = this.getFilesDir().getAbsolutePath() + "/" + faceXmlFileName;
        final String eyeXmlFilePath = this.getFilesDir().getAbsolutePath() + "/" + eyeXmlFileName;

        File faceXmlFile = new File(faceXmlFilePath);
        File eyeXmlFile = new File(eyeXmlFilePath);

        if (faceXmlFile.exists() && eyeXmlFile.exists()) {
            moveMainActivity();
            return true;
        } else {
            return false;
        }
    }

    /**
     * xml 파일 생성
     */
    private void getFaceDetectXml() {
        final String faceXmlFileName = "haarcascade_frontalface_alt.xml";
        final String eyeXmlFileName = "haarcascade_eye_tree_eyeglasses.xml";
        final String faceXmlFilePath = this.getFilesDir().getAbsolutePath() + "/" + faceXmlFileName;
        final String eyeXmlFilePath = this.getFilesDir().getAbsolutePath() + "/" + eyeXmlFileName;
        writeToFilePath(faceXmlFilePath, readFromAssets(faceXmlFileName));
        writeToFilePath(eyeXmlFilePath, readFromAssets(eyeXmlFileName));
    }

    /**
     * assets 디렉토리 내 파일을 읽어 String 으로 반환
     *
     * @param fileName xml 파일 이름
     * @return 파일 내용
     */
    private String readFromAssets(String fileName) {
        AssetManager manager = getResources().getAssets();
        InputStream stream = null;
        String xmlString = "";

        try {
            stream = manager.open(fileName);
            byte[] buf = new byte[stream.available()];

            if (stream.read(buf) >= 0) {
                xmlString = new String(buf);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (stream != null) {
                    stream.close();
                }
            } catch (Exception e1) {
                e1.printStackTrace();
            }
        }

        return xmlString;
    }

    /**
     * 주어진 경로에 파일 생성
     *
     * @param filePath 파일을 생성할 경로 및 파일명
     * @param content  파일에 추가할 내용
     */
    private void writeToFilePath(String filePath, String content) {
        File file = new File(filePath);
        FileWriter fileWriter = null;
        BufferedWriter bufferedWriter = null;

        try {
            fileWriter = new FileWriter(file, false);
            bufferedWriter = new BufferedWriter(fileWriter);
            bufferedWriter.append(content);
            bufferedWriter.close();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (fileWriter != null) {
                    fileWriter.close();
                }
                if (bufferedWriter != null) {
                    bufferedWriter.close();
                }
            } catch (Exception e1) {
                e1.printStackTrace();
            }
        }
    }

    private void moveMainActivity() {
        startActivity(new Intent(LogoActivity.this, MainActivity.class));
        finish();
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
    }
}
