package com.racersyun.studytracker;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.macroyau.blue2serial.BluetoothDeviceListDialog;
import com.macroyau.blue2serial.BluetoothSerial;
import com.macroyau.blue2serial.BluetoothSerialListener;

/**
 * Created by racer on 2016-08-20.
 */
public class BluetoothSettingActivity extends AppCompatActivity implements BluetoothSerialListener, BluetoothDeviceListDialog.OnDeviceSelectedListener {
    private static final int REQUEST_ENABLE_BLUETOOTH = 1;
    private boolean CRLF = false;
    private int[] i_BTDeviceReceivedValue = new int[5];
    private String str_BTDeviceReceivedRawValue;

    private BluetoothSerial BTSerial;
    private TextView tv_BTTerminalValueView, tv_BTStatusView;
    private LinearLayout ll_CushionLayoutView;

    private CushionViewClass cvc;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting_bluetooth);
        setTitle("스마트 방석 설정");
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        tv_BTTerminalValueView = (TextView) findViewById(R.id.tv_BTTerminalValueView);
        tv_BTStatusView = (TextView) findViewById(R.id.tv_BTStatusView);
        ll_CushionLayoutView = (LinearLayout) findViewById(R.id.ll_CushionLayoutView);

        cvc = new CushionViewClass(this, 290, 50, 500, 500);
        ll_CushionLayoutView.addView(cvc);

        BTSerial = new BluetoothSerial(this, this);
    }

    @Override
    protected void onStart() {
        super.onStart();
        BTSerial.setup();
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (BTSerial.checkBluetooth() && BTSerial.isBluetoothEnabled()) {
            if (!BTSerial.isConnected()) {
                BTSerial.start();
                BTDeviceAutoConnect();
            }
        }
        BTDeviceStatusUpdate();
    }

    @Override
    protected void onStop() {
        super.onStop();
        BTSerial.stop();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(this, MainActivity.class));
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case REQUEST_ENABLE_BLUETOOTH:
                if (resultCode == Activity.RESULT_OK) {
                    BTSerial.setup();
                }
                break;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void OnClick(View v) {
        switch (v.getId()) {
            case R.id.btn_BTConnectBtn:
                showDeviceListDialog();
                break;
            case R.id.btn_BTDisconnectBtn:
                BTSerial.stop();
                break;
            case R.id.btn_BTDeviceResetBtn:
                str_BTDeviceReceivedRawValue = "";
                tv_BTTerminalValueView.setText("센서 초기화 중..");
                SendDataToBTDevice("r");
                break;
            case R.id.btn_BtDeviceRequestBtn:
                str_BTDeviceReceivedRawValue = "";
                tv_BTTerminalValueView.setText("데이터 수신 중..");
                SendDataToBTDevice("q");
                break;
        }
    }

    private void BTDeviceAutoConnect() {
        SharedPreferences prefs = getSharedPreferences("sp_AutoBluetooth", MODE_PRIVATE);
        String autoPairedDeviceAddr = prefs.getString("spref_PairedDeviceAddr", "");
        Log.e("BTADDR", autoPairedDeviceAddr);
        if (!autoPairedDeviceAddr.equals("")) {
            BTSerial.connect(autoPairedDeviceAddr);
            BTDeviceStatusUpdate();
        }
    }

    private void BTDeviceStatusUpdate() {
        final int i_CurrentState;
        if (BTSerial != null) {
            i_CurrentState = BTSerial.getState();
        } else {
            i_CurrentState = BluetoothSerial.STATE_DISCONNECTED;
        }

        String str_CurrentStateMsg;
        switch (i_CurrentState) {
            case BluetoothSerial.STATE_CONNECTING:
                str_CurrentStateMsg = "연결중...";
                break;
            case BluetoothSerial.STATE_CONNECTED:
                str_CurrentStateMsg = BTSerial.getConnectedDeviceName() + " 연결됨";

                String autoPairedDeviceAddr = BTSerial.getConnectedDeviceAddress();
                SharedPreferences prefs = getSharedPreferences("sp_AutoBluetooth", MODE_PRIVATE);
                SharedPreferences.Editor editor = prefs.edit();
                editor.putString("spref_PairedDeviceAddr", autoPairedDeviceAddr);
                editor.commit();
                break;
            default:
                str_CurrentStateMsg = "연결된 장치 없음";
                break;
        }

        tv_BTStatusView.setText(str_CurrentStateMsg);
    }

    private void SendDataToBTDevice(String data) {
        if (data.length() > 0) {
            BTSerial.write(data, CRLF);
        }
    }

    private void ParseBTDataToInt(String data) {
        if (data.contains("Init")) {
            tv_BTTerminalValueView.setText("센서 초기화 완료");
        } else {
            str_BTDeviceReceivedRawValue = "";
            for (int i = 0; i < 5; i++) {
                int index = data.indexOf("#" + Integer.toString(i) + "#");
                String temp = data.substring(index + 3, data.substring(index + 3).indexOf("#") + index + 3);

                if (!b_IsInteger(temp)) {
                    temp = "0";
                }
                Log.e("BTPARSE", temp);
                i_BTDeviceReceivedValue[i] = Integer.parseInt(temp);
            }
            ParseBTDataToPercent();
        }
    }

    private boolean b_IsInteger(String data) {
        try {
            Integer.parseInt(data);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    private void ParseBTDataToPercent() {
        int i_SumOfDeviceValue = 1;

        str_BTDeviceReceivedRawValue = "";
        for (int i = 0; i < 5; i++) {
            i_SumOfDeviceValue += i_BTDeviceReceivedValue[i];
        }

        for (int i = 0; i < 5; i++) {
            i_BTDeviceReceivedValue[i] = (int) ((double) i_BTDeviceReceivedValue[i] / i_SumOfDeviceValue * 100);
        }

        tv_BTTerminalValueView.setText("데이터 수신 완료");
        cvc.setCushionData(i_BTDeviceReceivedValue);
        ll_CushionLayoutView.removeAllViews();
        ll_CushionLayoutView.addView(cvc);
    }

    private void showDeviceListDialog() {
        BluetoothDeviceListDialog dialog = new BluetoothDeviceListDialog(this);
        dialog.setOnDeviceSelectedListener(this);
        dialog.setTitle("연결할 장치를 선택하세요");
        dialog.setDevices(BTSerial.getPairedDevices());
        dialog.showAddress(true);
        dialog.show();
    }

    @Override
    public void onBluetoothNotSupported() {
        new AlertDialog.Builder(this)
                .setMessage("블루투스가 지원되지 않는 기기입니다")
                .setPositiveButton("확인", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                })
                .setCancelable(false)
                .show();
    }

    @Override
    public void onBluetoothDisabled() {
        Intent enableBluetooth = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
        startActivityForResult(enableBluetooth, REQUEST_ENABLE_BLUETOOTH);
    }

    @Override
    public void onBluetoothDeviceDisconnected() {
        invalidateOptionsMenu();
        BTDeviceStatusUpdate();
    }

    @Override
    public void onConnectingBluetoothDevice() {
        BTDeviceStatusUpdate();
    }

    @Override
    public void onBluetoothDeviceConnected(String name, String address) {
        invalidateOptionsMenu();
        BTDeviceStatusUpdate();
    }

    @Override
    public void onBluetoothSerialRead(String message) {
        str_BTDeviceReceivedRawValue += message;
        if (str_BTDeviceReceivedRawValue.contains("#!")) {
            ParseBTDataToInt(str_BTDeviceReceivedRawValue);
        }
    }

    @Override
    public void onBluetoothSerialWrite(String message) {
    }

    @Override
    public void onBluetoothDeviceSelected(BluetoothDevice device) {
        BTSerial.connect(device);
    }
}