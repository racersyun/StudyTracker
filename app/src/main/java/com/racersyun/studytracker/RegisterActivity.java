package com.racersyun.studytracker;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by racer on 2016-08-07.
 */
public class RegisterActivity extends AppCompatActivity {
    private EditText et_UserIDEdit, et_UserPWEdit, et_UserMailEdit;
    private TextView tv_UserIDDuplicateChkResultView;
    private RadioGroup rg_StudentChkGroup;
    private boolean b_isUserIDUseble = false;
    private boolean b_isRegisterSuccess = false;
    private String str_ErrorMsgToast = "";
    private NetworkServiceClass nsc = new NetworkServiceClass();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        setTitle("계정 등록하기");
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        et_UserIDEdit = (EditText) findViewById(R.id.et_UserIDEdit);
        et_UserPWEdit = (EditText) findViewById(R.id.et_UserPWEdit);
        et_UserMailEdit = (EditText) findViewById(R.id.et_UserMailEdit);
        tv_UserIDDuplicateChkResultView = (TextView) findViewById(R.id.tv_UserIDDuplicateChkResultView);
        rg_StudentChkGroup = (RadioGroup) findViewById(R.id.rg_StudentChkGroup);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_UserIDDuplicateChkBtn:
                if (et_UserIDEdit.getText().toString().equals("")) {
                    tv_UserIDDuplicateChkResultView.setTextColor(Color.parseColor("#990000"));
                    tv_UserIDDuplicateChkResultView.setText("사용할 계정을 입력하세요.");
                } else {
                    b_isUserIDUseble = nsc.b_SetDataForServer(nsc.SERVER_REQ_IS_ID_EXIST, et_UserIDEdit.getText().toString());

                    if (b_isUserIDUseble == true) {
                        tv_UserIDDuplicateChkResultView.setTextColor(Color.parseColor("#000099"));
                        tv_UserIDDuplicateChkResultView.setText("사용 가능한 계정입니다.");
                        setUseableEditText(et_UserIDEdit, false);
                    } else {
                        tv_UserIDDuplicateChkResultView.setTextColor(Color.parseColor("#990000"));
                        tv_UserIDDuplicateChkResultView.setText("사용할 수 없는 계정입니다.");
                        setUseableEditText(et_UserIDEdit, true);
                    }
                }
                break;
            case R.id.btn_UserRegisterFinishBtn:
                if (!emptyCheck()) {
                    showDialog(DIALOG_PROGRESS_ID);
                } else {
                    Toast.makeText(RegisterActivity.this, str_ErrorMsgToast, Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.btn_UserRegisterCancelBtn:
                AlertDialog.Builder alert_confirm = new AlertDialog.Builder(RegisterActivity.this);
                alert_confirm.setMessage("회원등록을 취소 하시겠습니까?").setCancelable(false).setPositiveButton("네",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                finish();
                            }
                        }).setNegativeButton("아니오",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                return;
                            }
                        });
                AlertDialog alert = alert_confirm.create();
                alert.show();
                break;
        }
    }

    private void doItRegister() {
        String str_isStudent = "true";

        if (rg_StudentChkGroup.getCheckedRadioButtonId() == R.id.rb_StudentSelectRdBtn) {
            str_isStudent = "true";
        } else {
            str_isStudent = "false";
        }

        String str_UserPWCrypted = BCrypt.hashpw(et_UserPWEdit.getText().toString(), BCrypt.gensalt(11));
        b_isRegisterSuccess = nsc.b_SetDataForServer(nsc.SERVER_REQ_REGISTER, et_UserIDEdit.getText().toString(),
                str_UserPWCrypted, str_isStudent, et_UserMailEdit.getText().toString());

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                RegisterSuccessChk(b_isRegisterSuccess);
            }
        });
    }

    private void RegisterSuccessChk(boolean b_isRegisterSuccess) {
        if (b_isRegisterSuccess == true) {
            Toast.makeText(RegisterActivity.this, "회원등록 성공", Toast.LENGTH_SHORT).show();
            finish();
        } else {
            Toast.makeText(RegisterActivity.this, "회원등록 실패", Toast.LENGTH_SHORT).show();
        }
    }

    private boolean emptyCheck() {
        boolean b_isEditEmpty = false;

        if (et_UserIDEdit.getText().toString().equals("")) {
            str_ErrorMsgToast += "사용할 계정을 입력해 주세요";
            b_isEditEmpty = true;
        }
        if (b_isUserIDUseble == false) {
            str_ErrorMsgToast += "\n계정 중복 확인을 해 주세요";
            b_isEditEmpty = true;
        }
        if (et_UserPWEdit.getText().toString().equals("")) {
            str_ErrorMsgToast += "\n비밀 번호를 입력해 주세요";
            b_isEditEmpty = true;
        }
        if (rg_StudentChkGroup.getCheckedRadioButtonId() == -1) {
            str_ErrorMsgToast += "\n학생 여부를 선택해 주세요";
            b_isEditEmpty = true;
        }
        if (et_UserMailEdit.getText().toString().equals("")) {
            str_ErrorMsgToast += "\n이메일 주소를 입력해 주세요";
            b_isEditEmpty = true;
        }
        return b_isEditEmpty;
    }

    private void setUseableEditText(EditText et, boolean useable) {
        et.setClickable(useable);
        et.setEnabled(useable);
        et.setFocusable(useable);
        et.setFocusableInTouchMode(useable);
    }

    private static final int DIALOG_PROGRESS_ID = 1;

    private class ProgressThread extends Thread {
        Handler mHandler;

        ProgressThread(Handler h) {
            mHandler = h;
        }

        public void run() {
            doItRegister();
            Message msg = mHandler.obtainMessage();
            mHandler.sendMessage(msg);
        }
    }

    final Handler handler = new Handler() {
        public void handleMessage(Message msg) {
            dismissDialog(DIALOG_PROGRESS_ID);
            removeDialog(DIALOG_PROGRESS_ID);
        }
    };

    protected Dialog onCreateDialog(int id) {
        switch (id) {
            case DIALOG_PROGRESS_ID:
                ProgressDialog progressDialog = new ProgressDialog(RegisterActivity.this);
                progressDialog.setMessage("처리중...");
                new ProgressThread(handler).start();
                return progressDialog;
        }
        return null;
    }

    @Override
    public void onBackPressed() {
        AlertDialog.Builder alert_confirm = new AlertDialog.Builder(RegisterActivity.this);
        alert_confirm.setMessage("회원등록을 취소 하시겠습니까?").setCancelable(false).setPositiveButton("네",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                }).setNegativeButton("아니오",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        return;
                    }
                });
        AlertDialog alert = alert_confirm.create();
        alert.show();
    }
}