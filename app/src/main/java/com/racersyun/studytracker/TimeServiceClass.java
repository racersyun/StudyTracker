package com.racersyun.studytracker;

import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.widget.TextView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * Created by racer on 2016-08-17.
 */
public class TimeServiceClass {
    final int MSG_START_TIMER = 0;
    final int MSG_STOP_TIMER = 1;
    final int MSG_UPDATE_TIMER = 2;
    final int MSG_RESUME_TIMER = 3;
    final int REFRESH_RATE = 50;

    Stopwatch timer = new Stopwatch();

    private boolean b_TimerHandlerMSGStatus = false;

    private TextView tv_TargetView;

    public TimeServiceClass() {
    }

    public TimeServiceClass(TextView tv_TargetView) {
        this.tv_TargetView = tv_TargetView;
    }

    Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case MSG_START_TIMER:
                    timer.start();
                    mHandler.sendEmptyMessage(MSG_UPDATE_TIMER);
                    b_TimerHandlerMSGStatus = true;
                    break;
                case MSG_UPDATE_TIMER:
                    tv_TargetView.setText(timer.getElapsedTime());
                    mHandler.sendEmptyMessageDelayed(MSG_UPDATE_TIMER, REFRESH_RATE);
                    b_TimerHandlerMSGStatus = true;
                    break;
                case MSG_STOP_TIMER:
                    mHandler.removeMessages(MSG_UPDATE_TIMER);
                    timer.stop();
                    tv_TargetView.setText(timer.getElapsedTime());
                    b_TimerHandlerMSGStatus = false;
                    break;
                case MSG_RESUME_TIMER:
                    timer.resume();
                    mHandler.sendEmptyMessage(MSG_UPDATE_TIMER);
                    b_TimerHandlerMSGStatus = true;
                default:
                    break;
            }
        }
    };

    protected void WatchMsgReceiver(int msg) {
        if (msg == MSG_START_TIMER) {
            mHandler.sendEmptyMessage(MSG_START_TIMER);
            b_TimerHandlerMSGStatus = true;
        } else if (msg == MSG_STOP_TIMER) {
            mHandler.sendEmptyMessage(MSG_STOP_TIMER);
            b_TimerHandlerMSGStatus = false;
        } else if (msg == MSG_RESUME_TIMER) {
            mHandler.sendEmptyMessage(MSG_RESUME_TIMER);
            b_TimerHandlerMSGStatus = true;
        }
    }

    protected boolean WatchMsgReturner() {
        return b_TimerHandlerMSGStatus;
    }

    protected class Stopwatch {
        private long startTime = 0;
        private long stopTime = 0;
        private long delayTime = 0;

        public void start() {
            this.startTime = System.currentTimeMillis();
        }

        public void stop() {
            this.stopTime = System.currentTimeMillis();
        }

        public void resume() {
            this.delayTime += System.currentTimeMillis() - this.stopTime;
        }

        public String getElapsedTime() {
            return getTimeFormat(System.currentTimeMillis() - startTime - delayTime);
        }

        public String getTimeFormat(long spandTime) {
            Calendar spandCalendar = Calendar.getInstance();
            spandCalendar.setTimeInMillis(spandTime);

            int i_Hour = spandCalendar.get(Calendar.HOUR_OF_DAY) - 9;
            int i_Minute = spandCalendar.get(Calendar.MINUTE);
            int i_Second = spandCalendar.get(Calendar.SECOND);

            String str_Hour = Integer.toString(i_Hour);
            String str_Minute = Integer.toString(i_Minute);
            String str_Second = Integer.toString(i_Second);

            if (i_Hour < 10) {
                str_Hour = "0" + i_Hour;
            }
            if (i_Minute < 10) {
                str_Minute = "0" + i_Minute;
            }
            if (i_Second < 10) {
                str_Second = "0" + i_Second;
            }
            return str_Hour + " : " + str_Minute + " : " + str_Second;
        }
    }

    protected String GetTimeDateForDisplay() {
        Date date_CurrentTime = new Date(System.currentTimeMillis());
        SimpleDateFormat sdf_FullDateFormat = new SimpleDateFormat("yyyy년 MM월 dd일 HH:mm:ss");
        return sdf_FullDateFormat.format(date_CurrentTime);
    }

    protected String GetDateForDisplay() {
        Date date_CurrentTime = new Date(System.currentTimeMillis());
        SimpleDateFormat sdf_FullDateFormat = new SimpleDateFormat("yyyy년 MM월 dd일");
        return sdf_FullDateFormat.format(date_CurrentTime);
    }

    protected String GetDateForSharedPreferences() {
        Date date_CurrentTime = new Date(System.currentTimeMillis());
        SimpleDateFormat sdf_FullDateFormat = new SimpleDateFormat("yyyyMMdd");
        return sdf_FullDateFormat.format(date_CurrentTime);
    }

    protected String GetLeftTimeForDisplay() {
        Calendar cal_Tomorrow = Calendar.getInstance();
        cal_Tomorrow.add(Calendar.DATE, 1);

        Date date_CurrentTime = new Date(System.currentTimeMillis());
        Date date_TomorrowTime = cal_Tomorrow.getTime();
        date_TomorrowTime.setHours(15);
        date_TomorrowTime.setMinutes(0);
        date_TomorrowTime.setSeconds(0);

        long long_CurrentTime = date_CurrentTime.getTime();
        long long_TomorrowTime = date_TomorrowTime.getTime();

        long long_RemainTime = long_TomorrowTime - long_CurrentTime;

        SimpleDateFormat sdf_FullDateFormat = new SimpleDateFormat("HH시간 mm분");

        return sdf_FullDateFormat.format(long_RemainTime);
    }

    protected String GetTimeForImage() {
        Date date_CurrentTime = new Date(System.currentTimeMillis());
        SimpleDateFormat sdf_FullDateFormat = new SimpleDateFormat("yyyyMMdd_HHmmss");
        return sdf_FullDateFormat.format(date_CurrentTime);
    }

    protected String GetFocusRate(String str_TotalTime, String str_FocusTime) {
        SimpleDateFormat sdf_OnlyTimeFormat = new SimpleDateFormat("HH : mm : ss", Locale.KOREA);
        Date date_TotalTime = null;
        Date date_FocusTime = null;

        try {
            date_TotalTime = sdf_OnlyTimeFormat.parse(str_TotalTime);
            date_FocusTime = sdf_OnlyTimeFormat.parse(str_FocusTime);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        long long_TotalTimeSec = ((9 * 60 * 60 * 1000) - Math.abs(date_TotalTime.getTime())) / 1000;
        long long_FocusTimeSec = ((9 * 60 * 60 * 1000) - Math.abs(date_FocusTime.getTime())) / 1000;

        String str_FocusRate = String.format("%.03f", (double) long_FocusTimeSec / long_TotalTimeSec * 100);

        return str_FocusRate;
    }

    protected String GetConvertedTimeForServer(String date) {
        SimpleDateFormat sdf_FullDateFormat = new SimpleDateFormat("yyyy년 MM월 dd일 HH:mm:ss");
        SimpleDateFormat sdf_ServerDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String str_ConvertedDate = null;

        try {
            Date date_OldDate = sdf_FullDateFormat.parse(date);
            str_ConvertedDate = sdf_ServerDateFormat.format(date_OldDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str_ConvertedDate;
    }

    protected String GetConvertedTimeForParent(String date) {
        SimpleDateFormat sdf_FullDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
        SimpleDateFormat sdf_ServerDateFormat = new SimpleDateFormat("HH:mm:ss");
        String str_ConvertedDate = null;

        try {
            Date date_OldDate = sdf_FullDateFormat.parse(date);
            str_ConvertedDate = sdf_ServerDateFormat.format(date_OldDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str_ConvertedDate;
    }

    protected int[] GetConvertedTimeForInt(String date) {
        SimpleDateFormat sdf_FullDateFormat = new SimpleDateFormat("HH : mm : ss");
        SimpleDateFormat sdf_HourFormat = new SimpleDateFormat("HH");
        SimpleDateFormat sdf_MinuteFormat = new SimpleDateFormat("mm");
        String str_ConvertedDate = null;
        int[] i_ConvertedTime = new int[2];

        Log.e("TIMEH1", date);
        try {
            Date date_OldDate = sdf_FullDateFormat.parse(date);
            str_ConvertedDate = sdf_HourFormat.format(date_OldDate);
            Log.e("TIMEH1", str_ConvertedDate);
            i_ConvertedTime[0] = Integer.valueOf(str_ConvertedDate);
            str_ConvertedDate = sdf_MinuteFormat.format(date_OldDate);
            Log.e("TIMEH2", str_ConvertedDate);
            i_ConvertedTime[1] = Integer.valueOf(str_ConvertedDate);

            Log.e("TIMEH4", Integer.toString(i_ConvertedTime[0]) + " " + Integer.toString(i_ConvertedTime[1]));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return i_ConvertedTime;
    }

    protected double GetStudyGoalSuccessRate(int i_goalHour, int i_goalMinute, int i_studiedHour, int i_studiedMinute) {
        double percentage;

        if ((i_goalHour == 0 && i_goalMinute == 0) || (i_studiedHour == 0 && i_studiedMinute == 0)) {
            return 0;
        }

        int i_goalTime = i_goalHour * 60 + i_goalMinute;
        int i_studiedTime = i_studiedHour * 60 + i_studiedMinute;

        percentage = ((double) i_studiedTime / i_goalTime) * 100.0;

        return Double.parseDouble(String.format("%.2f", percentage));
    }
}

