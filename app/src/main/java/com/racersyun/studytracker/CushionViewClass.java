package com.racersyun.studytracker;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.view.View;

/**
 * Created by racer on 2016-10-27.
 */

public class CushionViewClass extends View {
    int[] data = new int[5];
    int LeftTopX, LeftTopY, Width, Height;
    int x_Padding, y_Padding, sensorWidth, sensorHeight;

    public CushionViewClass(Context context, int LeftTopX, int LeftTopY, int Width, int Height) {
        super(context);

        this.LeftTopX = LeftTopX;
        this.LeftTopY = LeftTopY;
        this.Width = Width;
        this.Height = Height;

        this.sensorWidth = (int) ((double) Width / 3);
        this.sensorHeight = (int) ((double) Height / 3);
        this.x_Padding = (int) ((double) sensorWidth / 8);
        this.y_Padding = (int) ((double) sensorHeight / 8);
    }

    protected void setCushionData(int[] sensor) {
        for (int i = 0; i < 5; i++) {
            data[i] = sensor[i];
        }
    }

    private int[] setCushionColor(int sensordata) {
        int[] color = new int[3];

        if (sensordata < 5) {           // Black
            color[0] = 30;
            color[1] = 30;
            color[2] = 30;
        } else if (sensordata < 15) {   // Green
            color[0] = 0;
            color[1] = 255;
            color[2] = 0;
        } else if (sensordata < 30) {   // Yellow
            color[0] = 255;
            color[1] = 255;
            color[2] = 0;
        } else if (sensordata < 50) {   // Orange
            color[0] = 255;
            color[1] = 127;
            color[2] = 0;
        } else {        // Red
            color[0] = 255;
            color[1] = 0;
            color[2] = 0;
        }

        return color;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        drawCushion(canvas);
    }

    protected void drawCushion(Canvas canvas) {
        Paint paint = new Paint();
        paint.setAntiAlias(true);

        int[] color = new int[3];

        int textPaddingX = 8;
        int textPaddingY = 10;

        //-- 쿠션프레임 --//
        paint.setStyle(Paint.Style.FILL);
        paint.setColor(Color.argb(255, 0, 0, 0));
        canvas.drawRect(new Rect(LeftTopX, LeftTopY, LeftTopX + Width, LeftTopY + Height), paint);
        paint.setTextSize(35);

        // R-O-Y-G
        //-- Left Front --//
        color = setCushionColor(data[0]);
        paint.setStyle(Paint.Style.FILL);
        paint.setColor(Color.argb(255, color[0], color[1], color[2]));
        canvas.drawRect(new Rect(LeftTopX + x_Padding, LeftTopY + y_Padding, LeftTopX + sensorWidth - x_Padding, LeftTopY + sensorHeight - y_Padding), paint);
        canvas.drawText(Integer.toString(data[0]) + "%", (LeftTopX + sensorWidth - x_Padding) + textPaddingX, (LeftTopY + sensorHeight) - textPaddingY, paint);

        //-- Left Rear --//
        color = setCushionColor(data[2]);
        paint.setStyle(Paint.Style.FILL);
        paint.setColor(Color.argb(255, color[0], color[1], color[2]));
        canvas.drawRect(new Rect(LeftTopX + x_Padding, LeftTopY + sensorHeight + y_Padding, LeftTopX + sensorWidth - x_Padding, LeftTopY + 2 * sensorHeight - y_Padding), paint);
        canvas.drawText(Integer.toString(data[2]) + "%", (LeftTopX + sensorWidth - x_Padding) + textPaddingX, (LeftTopY + 2 * sensorHeight) - textPaddingY, paint);

        //-- Right Front --//
        color = setCushionColor(data[1]);
        paint.setStyle(Paint.Style.FILL);
        paint.setColor(Color.argb(255, color[0], color[1], color[2]));
        canvas.drawRect(new Rect(LeftTopX + 2 * sensorWidth + x_Padding, LeftTopY + y_Padding, LeftTopX + Width - x_Padding, LeftTopY + sensorHeight - y_Padding), paint);
        canvas.drawText(Integer.toString(data[1]) + "%", (LeftTopX + 2 * sensorWidth) - (6 * textPaddingX), (LeftTopY) + (5 * textPaddingY), paint);

        //-- Right Rear --//
        color = setCushionColor(data[4]);
        paint.setStyle(Paint.Style.FILL);
        paint.setColor(Color.argb(255, color[0], color[1], color[2]));
        canvas.drawRect(new Rect(LeftTopX + 2 * sensorWidth + x_Padding, LeftTopY + sensorHeight + y_Padding, LeftTopX + Width - x_Padding, LeftTopY + 2 * sensorHeight - y_Padding), paint);
        canvas.drawText(Integer.toString(data[4]) + "%", (LeftTopX + 2 * sensorWidth) - (6 * textPaddingX), (LeftTopY + sensorHeight) + (5 * textPaddingY), paint);

        //-- Center Rear --//
        color = setCushionColor(data[3]);
        paint.setStyle(Paint.Style.FILL);
        paint.setColor(Color.argb(255, color[0], color[1], color[2]));
        canvas.drawRect(new Rect(LeftTopX + sensorWidth + x_Padding, LeftTopY + 2 * sensorHeight + y_Padding, LeftTopX + 2 * sensorWidth - x_Padding, LeftTopY + Height - y_Padding), paint);
        canvas.drawText(Integer.toString(data[3]) + "%", (LeftTopX + 2 * sensorWidth) + textPaddingX * (float) 0.7, LeftTopY + Height - 3 * textPaddingY, paint);
    }
}
