package com.racersyun.studytracker;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Locale;
import java.util.Random;

/**
 * Created by racer on 2016-08-29.
 */
public class RemoteSettingActivity extends AppCompatActivity {
    private RelativeLayout rl_StudentLayoutView, rl_ParentLayoutView, rl_StudentRemoteSettingLayoutView;
    private TextView tv_LoginStatusView, tv_LoginModeView, tv_ConfirmNumView, tv_ConfirmNumNameView, tv_ConnectedAccountView;
    private EditText et_ConnectingAccountEdit, et_ConfirmNumInputEdit;
    private Button btn_ConfirmNumChkBtn;
    private Switch sw_AllowRemoteConnSW;

    private String str_CurrentUserID;
    private boolean b_isSwitchEnabled = false;
    private boolean b_isRemoteSuccess = false;
    private boolean b_isStudentOrParent = false;
    private NetworkServiceClass nsc = new NetworkServiceClass();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting_remote);
        setTitle("원격 접속 설정");
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        rl_StudentLayoutView = (RelativeLayout) findViewById(R.id.rl_StudentLayoutView);
        rl_ParentLayoutView = (RelativeLayout) findViewById(R.id.rl_ParentLayoutView);
        rl_StudentRemoteSettingLayoutView = (RelativeLayout) findViewById(R.id.rl_StudentRemoteSettingLayoutView);

        tv_LoginModeView = (TextView) findViewById(R.id.tv_LoginModeView);
        tv_LoginStatusView = (TextView) findViewById(R.id.tv_LoginStatusView);
        tv_ConfirmNumView = (TextView) findViewById(R.id.tv_ConfirmNumView);
        tv_ConfirmNumNameView = (TextView) findViewById(R.id.tv_ConfirmNumNameView);
        tv_ConnectedAccountView = (TextView) findViewById(R.id.tv_ConnectedAccountView);

        et_ConnectingAccountEdit = (EditText) findViewById(R.id.et_ConnectingAccountEdit);
        et_ConfirmNumInputEdit = (EditText) findViewById(R.id.et_ConfirmNumInputEdit);
        sw_AllowRemoteConnSW = (Switch) findViewById(R.id.sw_AllowRemoteConnSW);
        sw_AllowRemoteConnSW.setOnCheckedChangeListener(swlistener);
        btn_ConfirmNumChkBtn = (Button) findViewById(R.id.btn_ConfirmNumChkBtn);

        SetLayoutView();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public Switch.OnCheckedChangeListener swlistener = new Switch.OnCheckedChangeListener() {
        public void onCheckedChanged(CompoundButton arg0, boolean arg1) {
            b_isSwitchEnabled = arg1;
            student_isRemoteEnabled();
        }
    };

    private void SetLayoutView() {
        SharedPreferences prefs = getSharedPreferences("sp_LoginStatusCheck", MODE_PRIVATE);

        if (prefs.getBoolean("spref_isAlreadyLogin", false)) {
            if (prefs.getBoolean("spref_isAutoLoginEnable", false)) {
                str_CurrentUserID = prefs.getString("spref_AutoLoginUserID", "");
            } else {
                str_CurrentUserID = prefs.getString("spref_ManualLoginUserID", "");
            }

            b_isStudentOrParent = nsc.b_SetDataForServer(nsc.SERVER_REQ_IS_STUDENT, str_CurrentUserID);
            tv_LoginStatusView.setText(str_CurrentUserID + " 로 로그인 됨");

            if (b_isStudentOrParent) {
                StudentLayoutView();
            } else {
                ParentLayoutView();
            }
        } else {
            tv_LoginStatusView.setText("로그인 실패");
            tv_LoginModeView.setText("로그인 실패");
            sw_AllowRemoteConnSW.setEnabled(false);
        }
    }

    private void StudentLayoutView() {
        rl_StudentLayoutView.setVisibility(View.VISIBLE);
        rl_ParentLayoutView.setVisibility(View.GONE);
        tv_LoginModeView.setText("학생 모드");

        student_isRemoteEnabled();
    }

    private void student_isRemoteEnabled() {
        boolean b_isRemoteLogined = nsc.b_SetDataForServer(nsc.SERVER_REQ_IS_REMOTED_S, str_CurrentUserID);

        if (b_isRemoteLogined) {
            sw_AllowRemoteConnSW.setChecked(true);
            rl_StudentRemoteSettingLayoutView.setVisibility(View.VISIBLE);
            tv_ConfirmNumNameView.setText("원격 접속됨");
            tv_ConfirmNumView.setVisibility(View.GONE);

            ArrayList<ArrayList<String>> list = nsc.str_SetDataForServer(nsc.SERVER_REQ_IS_REMOTED_S, str_CurrentUserID);
            String str_ConnectedParentUserID = list.get(0).get(0).toString();

            tv_ConnectedAccountView.setText(str_ConnectedParentUserID);
        } else if (!b_isRemoteLogined && b_isSwitchEnabled) {
            rl_StudentRemoteSettingLayoutView.setVisibility(View.VISIBLE);
            Random random = new Random();
            int i_CertifyNum = random.nextInt(10000);
            tv_ConfirmNumView.setText(String.format(Locale.ENGLISH, "%04d", i_CertifyNum));
            nsc.b_SetDataForServer(nsc.SERVER_REQ_SETCERTIFYNUM, str_CurrentUserID, String.valueOf(i_CertifyNum));
        } else {
            rl_StudentRemoteSettingLayoutView.setVisibility(View.GONE);
        }

        if (b_isRemoteLogined && !b_isSwitchEnabled) {
            rl_StudentRemoteSettingLayoutView.setVisibility(View.GONE);
            nsc.b_SetDataForServer(nsc.SERVER_REQ_RMVREMOTEID, str_CurrentUserID, tv_ConnectedAccountView.getText().toString());
            sw_AllowRemoteConnSW.setChecked(false);
        }
    }

    private void ParentLayoutView() {
        rl_StudentLayoutView.setVisibility(View.GONE);
        rl_ParentLayoutView.setVisibility(View.VISIBLE);
        tv_LoginModeView.setText("학습 관리자 모드");

        parent_RequestRemoteConnect();
    }

    private void parent_RequestRemoteConnect() {
        parent_isRemoteEnabled();

        if (!b_isRemoteSuccess) {
            b_isRemoteSuccess = nsc.b_SetDataForServer(nsc.SERVER_REQ_CHKCERTIFYNUM, str_CurrentUserID, et_ConfirmNumInputEdit.getText().toString(), et_ConnectingAccountEdit.getText().toString());

            if (b_isRemoteSuccess) {
                nsc.b_SetDataForServer(nsc.SERVER_REQ_SETREMOTEID, str_CurrentUserID, et_ConnectingAccountEdit.getText().toString());
                nsc.b_SetDataForServer(nsc.SERVER_REQ_RMVCERTIFYNUM, et_ConnectingAccountEdit.getText().toString());
                Toast.makeText(this, "원격 접속됨", Toast.LENGTH_SHORT).show();
                parent_isRemoteEnabled();
            }
        }
    }

    private void parent_isRemoteEnabled() {
        b_isRemoteSuccess = nsc.b_SetDataForServer(nsc.SERVER_REQ_IS_REMOTED_P, str_CurrentUserID);

        if (b_isRemoteSuccess) {
            ArrayList<ArrayList<String>> list = nsc.str_SetDataForServer(nsc.SERVER_REQ_IS_REMOTED_P, str_CurrentUserID);
            String str_ConnectedStudentUserID = list.get(0).get(0).toString();
            et_ConnectingAccountEdit.setText(str_ConnectedStudentUserID);
            et_ConnectingAccountEdit.setEnabled(false);
            et_ConfirmNumInputEdit.setEnabled(false);
            et_ConfirmNumInputEdit.setHint("연결됨");
            btn_ConfirmNumChkBtn.setEnabled(false);
        } else {
            et_ConnectingAccountEdit.setEnabled(true);
            et_ConfirmNumInputEdit.setEnabled(true);
            et_ConfirmNumInputEdit.setHint("인증번호 입력");
            btn_ConfirmNumChkBtn.setEnabled(true);
        }
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_RemoteSettingFinishBtn:
                if (b_isStudentOrParent) {
                    nsc.b_SetDataForServer(nsc.SERVER_REQ_RMVCERTIFYNUM, str_CurrentUserID);
                }
                startActivity(new Intent(this, MainActivity.class));
                finish();
                break;
            case R.id.btn_ConfirmNumChkBtn:
                parent_RequestRemoteConnect();
                break;
        }
    }

    @Override
    public void onBackPressed() {
        if (b_isStudentOrParent) {
            nsc.b_SetDataForServer(nsc.SERVER_REQ_RMVCERTIFYNUM, str_CurrentUserID);
        }

        startActivity(new Intent(this, MainActivity.class));
        finish();
    }
}
