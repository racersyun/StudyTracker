package com.racersyun.studytracker;

import android.content.Intent;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.MenuItem;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;

/**
 * Created by racer on 2016-08-21.
 */
public class CameraSettingActivity extends AppCompatActivity {
    private CameraServiceClass csc;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting_camera);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        setTitle("카메라 확인");

        CamView();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(this, MainActivity.class));
    }

    private void CamView() {
        ImageView iv_CameraView = new ImageView(this);
        SurfaceView camView = new SurfaceView(this);

        int PreviewSizeWidth = 640;
        int PreviewSizeHeight = 480;
        DisplayMetrics displayMertics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMertics);

        SurfaceHolder camHolder = camView.getHolder();
        csc = new CameraServiceClass(this, PreviewSizeWidth, PreviewSizeHeight, iv_CameraView, CameraServiceClass.CSC_MODE_SETTING);

        camHolder.addCallback(csc);
        camHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);

        FrameLayout fl_CameraLayoutView;
        fl_CameraLayoutView = (FrameLayout) findViewById(R.id.fl_CameraLayoutView);
        fl_CameraLayoutView.addView(camView);
        fl_CameraLayoutView.addView(iv_CameraView);
    }

    protected void onPause() {
        if (csc != null)
            csc.onPause();
        super.onPause();
    }
}