package com.racersyun.studytracker;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.navigation.NavigationView;
import com.google.android.material.snackbar.Snackbar;

import java.io.File;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    private static final int DIALOG_PROGRESS_ID = 1;

    private FloatingActionButton fab_StudyRefreshFab, fab_StudyStartFab;
    private EditText et_SubjectNameEdit;
    private RelativeLayout rl_StudentLayoutView, rl_ParentLayoutView, rl_ParentLayoutViewFail;
    private TextView tv_LoginedIDView, tv_LoginedEmailView, tv_LoginedModeView,
            tv_ParentSubjectNameView, tv_ParentStartTimeView, tv_ParentProceedTimeView, tv_StudyImgViewErrMsg,
            tv_TodayDateView, tv_TodayStudiedTimeView, tv_TargetSuccessPercentView, tv_TodayRemainTimeView;

    private String str_UserID = "";

    private final NetworkServiceClass nsc = new NetworkServiceClass();
    private final TimeServiceClass tsc = new TimeServiceClass();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        setTitle("Study Tracker");
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        View nv = navigationView.getHeaderView(0);
        tv_LoginedIDView = (TextView) nv.findViewById(R.id.tv_LoginedIDView);
        tv_LoginedEmailView = (TextView) nv.findViewById(R.id.tv_LoginedEmailView);
        tv_LoginedModeView = (TextView) nv.findViewById(R.id.tv_LoginedModeView);

        tv_LoginedEmailView.setVisibility(View.GONE);

        View abm = (View) findViewById(R.id.app_bar_main);

        fab_StudyStartFab = (FloatingActionButton) abm.findViewById(R.id.fab_StudyStartFab);
        fab_StudyRefreshFab = (FloatingActionButton) abm.findViewById(R.id.fab_StudyRefreshFab);

        rl_StudentLayoutView = (RelativeLayout) abm.findViewById(R.id.rl_StudentLayoutView);
        rl_ParentLayoutView = (RelativeLayout) abm.findViewById(R.id.rl_ParentLayoutView);
        rl_ParentLayoutViewFail = (RelativeLayout) abm.findViewById(R.id.rl_ParentLayoutViewFail);

        tv_ParentSubjectNameView = (TextView) abm.findViewById(R.id.tv_SubjectView);
        tv_ParentStartTimeView = (TextView) abm.findViewById(R.id.tv_StartTimeView);
        tv_ParentProceedTimeView = (TextView) abm.findViewById(R.id.tv_ProceedTimeView);
        tv_StudyImgViewErrMsg = (TextView) abm.findViewById(R.id.tv_StudyImgViewErrMsg);

        tv_TodayDateView = (TextView) abm.findViewById(R.id.tv_TodayDateView);
        tv_TodayStudiedTimeView = (TextView) abm.findViewById(R.id.tv_TodayStudiedTimeView);
        tv_TargetSuccessPercentView = (TextView) abm.findViewById(R.id.tv_TargetSuccessPercentView);
        tv_TodayRemainTimeView = (TextView) abm.findViewById(R.id.tv_TodayRemainTimeView);
        et_SubjectNameEdit = (EditText) abm.findViewById(R.id.et_SubjectNameEdit);

        LoginStatusChk();
    }

    public void onClickFab(View v) {
        switch (v.getId()) {
            case R.id.fab_StudyStartFab:
                if (et_SubjectNameEdit.getText().toString().equals("")) {
                    Snackbar.make(v, "어떤 공부를 할지 써 주셔야죠!", Snackbar.LENGTH_LONG).setAction("Action", null).show();
                } else {
                    Intent intent = new Intent(MainActivity.this, ProceedActivity.class);
                    intent.putExtra("it_SubjectName", et_SubjectNameEdit.getText().toString());
                    intent.putExtra("it_StudyStartTime", tsc.GetTimeDateForDisplay());
                    intent.putExtra("it_UserID", str_UserID);
                    startActivity(intent);
                    finish();
                    overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
                }
                break;
            case R.id.fab_StudyRefreshFab:
                ParentLayoutView();
                break;
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            SharedPreferences prefs = getSharedPreferences("sp_LoginStatusCheck", MODE_PRIVATE);
            SharedPreferences.Editor editor = prefs.edit();
            editor.putString("spref_ManualLoginUserID", "");
            editor.putString("spref_ManualLoginUserPW", "");
            editor.putBoolean("spref_isAlreadyLogin", false);
            editor.apply();
            finish();
            super.onBackPressed();
        }
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.nav_checkStudyList) {
            Intent intent = new Intent(MainActivity.this, StudyListActivity.class);
            intent.putExtra("it_UserID", str_UserID);
            startActivity(intent);
            finish();
        } else if (id == R.id.nav_setStudyGoal) {
            SharedPreferences prefs = getSharedPreferences("sp_StudyGoal" + str_UserID, MODE_PRIVATE);
            int i_goalHour = prefs.getInt("spref_goalHour", 0);
            int i_goalMinute = prefs.getInt("spref_goalMinute", 0);
            TimePickerDialog tpd = new TimePickerDialog(this, listener, i_goalHour, i_goalMinute, true);
            tpd.show();
        } else if (id == R.id.nav_loginSettings) {
            startActivity(new Intent(MainActivity.this, LoginActivity.class));
            finish();
        } else if (id == R.id.nav_bluetoothSettings) {
            startActivity(new Intent(MainActivity.this, BluetoothSettingActivity.class));
            finish();
        } else if (id == R.id.nav_cameraSettings) {
            startActivity(new Intent(MainActivity.this, CameraSettingActivity.class));
            finish();
        } else if (id == R.id.nav_remoteSettings) {
            startActivity(new Intent(MainActivity.this, RemoteSettingActivity.class));
            finish();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private final TimePickerDialog.OnTimeSetListener listener = new TimePickerDialog.OnTimeSetListener() {
        @Override
        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
            Toast.makeText(getApplicationContext(), "목표를 달성하기를 기대할게요!", Toast.LENGTH_SHORT).show();
            tv_TargetSuccessPercentView.setText(hourOfDay + "시간 " + minute + "분");

            SharedPreferences prefs = getSharedPreferences("sp_StudyGoal" + str_UserID, MODE_PRIVATE);
            SharedPreferences.Editor editor = prefs.edit();
            editor.putInt("spref_goalHour", hourOfDay);
            editor.putInt("spref_goalMinute", minute);
            editor.apply();
        }
    };

    private void LoginStatusChk() {
        SharedPreferences prefs = getSharedPreferences("sp_LoginStatusCheck", MODE_PRIVATE);
        boolean b_isStudent = true;
        boolean b_isLogined = prefs.getBoolean("spref_isAlreadyLogin", false);
        boolean b_isAutoLoginEnabled = prefs.getBoolean("spref_isAutoLoginEnable", false);

        if (b_isLogined) {
            if (b_isAutoLoginEnabled) {
                str_UserID = prefs.getString("spref_AutoLoginUserID", "");
            } else {
                str_UserID = prefs.getString("spref_ManualLoginUserID", "");
            }
            b_isStudent = nsc.b_SetDataForServer(NetworkServiceClass.SERVER_REQ_IS_STUDENT, str_UserID);
        } else {
            if (b_isAutoLoginEnabled) {
                boolean b_isLogin_success = nsc.b_SetDataForServer(NetworkServiceClass.SERVER_REQ_LOGIN, prefs.getString("spref_AutoLoginUserID", ""), prefs.getString("spref_AutoLoginUserPW", ""));

                if (b_isLogin_success) {
                    SharedPreferences.Editor editor = prefs.edit();
                    editor.putBoolean("spref_isAlreadyLogin", true);
                    editor.apply();

                    str_UserID = prefs.getString("spref_AutoLoginUserID", "");
                    b_isStudent = nsc.b_SetDataForServer(NetworkServiceClass.SERVER_REQ_IS_STUDENT, str_UserID);
                }
            }
        }

        if (str_UserID.equals("")) {
            str_UserID = "Guest";
        }
        tv_LoginedIDView.setText(str_UserID);

        if (b_isStudent) {
            tv_LoginedModeView.setText("학생");
            StudentLayoutView();
        } else {
            tv_LoginedModeView.setText("학부모");
            File oldImg = new File("/sdcard/Android/data/StudyTrackerData/IMG/cache.jpg");
            if (oldImg.exists()) {
                oldImg.delete();
            }

            ParentLayoutView();
        }
    }

    private void StudentLayoutView() {
        rl_ParentLayoutView.setVisibility(View.GONE);
        rl_StudentLayoutView.setVisibility(View.VISIBLE);
        rl_ParentLayoutViewFail.setVisibility(View.GONE);

        fab_StudyStartFab.setVisibility(View.VISIBLE);
        fab_StudyRefreshFab.setVisibility(View.GONE);

        tv_TodayDateView.setText(tsc.GetDateForDisplay());
        tv_TodayRemainTimeView.setText(tsc.GetLeftTimeForDisplay() + " 남았어요!");

        SharedPreferences prefs2 = getSharedPreferences("sp_TodayStudyTime" + str_UserID + tsc.GetDateForSharedPreferences(), MODE_PRIVATE);
        int i_studiedHour = prefs2.getInt("spref_studyHour", 0);
        int i_studiedMinute = prefs2.getInt("spref_studyMinute", 0);

        if (i_studiedHour == 0 && i_studiedMinute == 0) {
            tv_TodayStudiedTimeView.setText("아직 공부 안했어요" + new String(Character.toChars(0x1F605)));
        } else if (i_studiedHour == 0) {
            tv_TodayStudiedTimeView.setText(i_studiedMinute + "분 공부했어요!");
        } else {
            tv_TodayStudiedTimeView.setText(i_studiedHour + "시간 " + i_studiedMinute + "분 공부했어요!");
        }

        SharedPreferences prefs = getSharedPreferences("sp_StudyGoal" + str_UserID, MODE_PRIVATE);
        int i_goalHour = prefs.getInt("spref_goalHour", 0);
        int i_goalMinute = prefs.getInt("spref_goalMinute", 0);

        if (i_goalHour == 0 && i_goalMinute == 0) {
            tv_TargetSuccessPercentView.setText("목표가 설정되지 않았어요!");
        } else {
            tv_TargetSuccessPercentView.setText(tsc.GetStudyGoalSuccessRate(i_goalHour, i_goalMinute, i_studiedHour, i_studiedMinute) + "%");
        }
    }

    private void ParentLayoutView() {
        rl_StudentLayoutView.setVisibility(View.GONE);
        rl_ParentLayoutView.setVisibility(View.VISIBLE);
        rl_ParentLayoutViewFail.setVisibility(View.GONE);

        fab_StudyStartFab.setVisibility(View.GONE);
        fab_StudyRefreshFab.setVisibility(View.VISIBLE);

        showDialog(DIALOG_PROGRESS_ID);

        File NewImg = new File("/sdcard/Android/data/StudyTrackerData/IMG/cache.jpg");

        if (NewImg.exists()) {
            tv_StudyImgViewErrMsg.setVisibility(View.GONE);
            Bitmap ImgBitmap = BitmapFactory.decodeFile(NewImg.getAbsolutePath());
            ImageView iv_StudyImgView = (ImageView) findViewById(R.id.iv_StudyImgView);
            iv_StudyImgView.setImageBitmap(ImgBitmap);
        } else {
            tv_StudyImgViewErrMsg.setVisibility(View.VISIBLE);
        }
    }

    private class ProgressThread extends Thread {
        Handler mHandler;

        ProgressThread(Handler h) {
            mHandler = h;
        }

        public void run() {
            ArrayList<ArrayList<String>> list = nsc.str_SetDataForServer(NetworkServiceClass.SERVER_REQ_IS_REMOTED_P, str_UserID);
            Log.e("SERVER", list.toString());
            //!list.isEmpty() || !list.get(0).isEmpty() || !list.get(0).toString().equals("")
            if (list.get(0).size() != 0) {
                Log.e("SERVER", Integer.toString(list.size()));
                Log.e("SERVER", Integer.toString(list.get(0).size()));
                final ArrayList<ArrayList<String>> list2 = nsc.str_SetDataForServer(NetworkServiceClass.SERVER_REQ_GETSTUDYINFO, list.get(0).get(0).toString());
                boolean isStudied = !list2.get(0).isEmpty();
                isStudyProceeding(isStudied, list2);
                Log.e("SERVER", Boolean.toString(isStudied));
            } else {
                runOnUiThread(() -> {
                    rl_ParentLayoutView.setVisibility(View.GONE);
                    rl_ParentLayoutViewFail.setVisibility(View.VISIBLE);
                });
            }
            Message msg = mHandler.obtainMessage();
            mHandler.sendMessage(msg);
        }
    }

    final Handler handler = new Handler() {
        public void handleMessage(Message msg) {
            dismissDialog(DIALOG_PROGRESS_ID);
            removeDialog(DIALOG_PROGRESS_ID);
        }
    };

    protected Dialog onCreateDialog(int id) {
        switch (id) {
            case DIALOG_PROGRESS_ID:
                ProgressDialog progressDialog = new ProgressDialog(MainActivity.this);

                new MainActivity.ProgressThread(handler).start();
                return progressDialog;
        }
        return null;
    }

    private void isStudyProceeding(boolean isEnabled, final ArrayList<ArrayList<String>> list2) {
        if (isEnabled) {
            Log.e("SERVER", list2.get(0).get(0));
            nsc.PhotoDownload(list2.get(0).get(0));

            runOnUiThread(() -> {
                tv_ParentSubjectNameView.setText(list2.get(0).get(1));
                tv_ParentStartTimeView.setText(list2.get(0).get(2));
                tv_ParentProceedTimeView.setText(tsc.GetConvertedTimeForParent(list2.get(0).get(3).toString()));
            });
        } else {
            runOnUiThread(() -> {
                tv_ParentSubjectNameView.setText("지금은 공부중이 아닙니다!");
                tv_ParentStartTimeView.setText("");
                tv_ParentProceedTimeView.setText("");
            });
        }
    }
}
