package com.racersyun.studytracker;

import android.os.StrictMode;
import android.util.Log;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by racer on 2016-08-16.
 */
public class NetworkServiceClass {
    final static int SERVER_REQ_LOGIN = 1;          // 로그인
    final static int SERVER_REQ_REGISTER = 2;       // 회원등록
    final static int SERVER_REQ_IS_ID_EXIST = 3;      // 계정 중복 확인
    final static int SERVER_REQ_IS_STUDENT = 4;      // 계정 학생/관리자 확인
    final static int SERVER_REQ_IS_REMOTED_S = 5;    // 원격 접속 여부 확인(학생)
    final static int SERVER_REQ_IS_REMOTED_P = 6;    // 원격 접속 여부 확인(관리자)
    final static int SERVER_REQ_SETCERTIFYNUM = 7;  // 원격 인증번호 추가(학생)
    final static int SERVER_REQ_CHKCERTIFYNUM = 8;  // 원격 인증번호 확인(관리자)
    final static int SERVER_REQ_RMVCERTIFYNUM = 9;  // 원격 인증번호 삭제
    final static int SERVER_REQ_SETREMOTEID = 10;   // 원격 연결 계정 추가
    final static int SERVER_REQ_RMVREMOTEID = 11;   // 원격 연결 계정 삭제
    final static int SERVER_REQ_ADDSTUDY = 12;      // 학습 종료 정보 추가
    final static int SERVER_REQ_UPDATESTUDYINFO = 13;   // 학습 진행 정보 추가
    final static int SERVER_REQ_RMVSTUDYINFO = 14;  // 학습 종료시 진행정보 삭제
    final static int SERVER_REQ_GETSTUDYINFO = 15;  // 학습 진행정보 수집
    final static int SERVER_REQ_GETSTUDIEDLIST = 16;

    //- 서버 통신 사전설정 > T/F 반환 -//
    protected boolean b_SetDataForServer(int i_RequestServerMode, String... strs_Data) {
        String str_ServerURL = "";
        boolean b_DBResult;
        ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();

        switch (i_RequestServerMode) {
            case SERVER_REQ_LOGIN:
                str_ServerURL = "http://your_domain_name/StudyTracker/LOGIN.jsp";
                nameValuePairs.add(new BasicNameValuePair("uid_from_android", strs_Data[0]));
                nameValuePairs.add(new BasicNameValuePair("upw_from_android", strs_Data[1]));
                break;
            case SERVER_REQ_REGISTER:
                str_ServerURL = "http://your_domain_name/StudyTracker/REGISTER.jsp";
                nameValuePairs.add(new BasicNameValuePair("uid_from_android", strs_Data[0]));
                nameValuePairs.add(new BasicNameValuePair("upw_from_android", strs_Data[1]));
                nameValuePairs.add(new BasicNameValuePair("iss_from_android", strs_Data[2]));
                nameValuePairs.add(new BasicNameValuePair("ema_from_android", strs_Data[3]));
                break;
            case SERVER_REQ_IS_ID_EXIST:
                str_ServerURL = "http://your_domain_name/StudyTracker/ISIDEXIST.jsp";
                nameValuePairs.add(new BasicNameValuePair("uid_from_android", strs_Data[0]));
                break;
            case SERVER_REQ_IS_STUDENT:
                str_ServerURL = "http://your_domain_name/StudyTracker/ISSTUDENT.jsp";
                nameValuePairs.add(new BasicNameValuePair("uid_from_android", strs_Data[0]));
                break;
            case SERVER_REQ_IS_REMOTED_S:
                str_ServerURL = "http://your_domain_name/StudyTracker/ISREMOTED_S.jsp";
                nameValuePairs.add(new BasicNameValuePair("uid_from_android", strs_Data[0]));
                break;
            case SERVER_REQ_IS_REMOTED_P:
                str_ServerURL = "http://your_domain_name/StudyTracker/ISREMOTED_P.jsp";
                nameValuePairs.add(new BasicNameValuePair("uid_from_android", strs_Data[0]));
                break;
            case SERVER_REQ_SETCERTIFYNUM:
                str_ServerURL = "http://your_domain_name/StudyTracker/SETCERTIFYNUM.jsp";
                nameValuePairs.add(new BasicNameValuePair("uid_from_android", strs_Data[0]));
                nameValuePairs.add(new BasicNameValuePair("randnum_from_android", strs_Data[1]));
                break;
            case SERVER_REQ_CHKCERTIFYNUM:
                str_ServerURL = "http://your_domain_name/StudyTracker/CHKCERTIFYNUM.jsp";
                nameValuePairs.add(new BasicNameValuePair("uid_from_android", strs_Data[0]));
                nameValuePairs.add(new BasicNameValuePair("randnum_from_android", strs_Data[1]));
                nameValuePairs.add(new BasicNameValuePair("tgtid_from_android", strs_Data[2]));
                break;
            case SERVER_REQ_RMVCERTIFYNUM:
                str_ServerURL = "http://your_domain_name/StudyTracker/RMVCERTIFYNUM.jsp";
                nameValuePairs.add(new BasicNameValuePair("uid_from_android", strs_Data[0]));
                break;
            case SERVER_REQ_SETREMOTEID:
                str_ServerURL = "http://your_domain_name/StudyTracker/SETREMOTEID.jsp";
                nameValuePairs.add(new BasicNameValuePair("uid_from_android", strs_Data[0]));
                nameValuePairs.add(new BasicNameValuePair("tgtid_from_android", strs_Data[1]));
                break;
            case SERVER_REQ_RMVREMOTEID:
                str_ServerURL = "http://your_domain_name/StudyTracker/RMVREMOTEID.jsp";
                nameValuePairs.add(new BasicNameValuePair("uid_from_android", strs_Data[0]));
                nameValuePairs.add(new BasicNameValuePair("tgtid_from_android", strs_Data[1]));
                break;
            case SERVER_REQ_ADDSTUDY:
                str_ServerURL = "http://your_domain_name/StudyTracker/ADDSTUDY.jsp";
                b_SetDataForServer(SERVER_REQ_RMVSTUDYINFO, strs_Data[0]);
                nameValuePairs.add(new BasicNameValuePair("uid_from_android", strs_Data[0]));
                nameValuePairs.add(new BasicNameValuePair("subjectname_from_android", strs_Data[1]));
                nameValuePairs.add(new BasicNameValuePair("starttime_from_android", strs_Data[2]));
                nameValuePairs.add(new BasicNameValuePair("finishtime_from_android", strs_Data[3]));
                nameValuePairs.add(new BasicNameValuePair("totaltime_from_android", strs_Data[4]));
                nameValuePairs.add(new BasicNameValuePair("concenttime_from_android", strs_Data[5]));
                nameValuePairs.add(new BasicNameValuePair("concentrate_from_android", strs_Data[6]));
                break;
            case SERVER_REQ_UPDATESTUDYINFO:
                str_ServerURL = "http://your_domain_name/StudyTracker/UPDATESTUDYINFO.jsp";
                nameValuePairs.add(new BasicNameValuePair("uid_from_android", strs_Data[0]));
                nameValuePairs.add(new BasicNameValuePair("subjectname_from_android", strs_Data[1]));
                nameValuePairs.add(new BasicNameValuePair("starttime_from_android", strs_Data[2]));
                nameValuePairs.add(new BasicNameValuePair("totaltime_from_android", strs_Data[3]));
                nameValuePairs.add(new BasicNameValuePair("jpgfilename_from_android", strs_Data[4]));
                break;
            case SERVER_REQ_RMVSTUDYINFO:
                str_ServerURL = "http://your_domain_name/StudyTracker/RMVSTUDYINFO.jsp";
                nameValuePairs.add(new BasicNameValuePair("uid_from_android", strs_Data[0]));
                break;
        }

        b_DBResult = b_ServerResultParse(str_RequestContactToServer(str_ServerURL, nameValuePairs));
        return b_DBResult;
    }

    //- 서버 통신 사전설정 > String List 반환 -//
    protected ArrayList<ArrayList<String>> str_SetDataForServer(int i_RequestServerMode, String... strs_Data) {
        String str_ServerURL = "";
        ArrayList<ArrayList<String>> lst_DBResult = new ArrayList<ArrayList<String>>();
        ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();

        switch (i_RequestServerMode) {
            case SERVER_REQ_IS_REMOTED_S:
                str_ServerURL = "http://your_domain_name/StudyTracker/str_ISREMOTED_S.jsp";
                nameValuePairs.add(new BasicNameValuePair("uid_from_android", strs_Data[0]));
                break;
            case SERVER_REQ_IS_REMOTED_P:
                str_ServerURL = "http://your_domain_name/StudyTracker/str_ISREMOTED_P.jsp";
                nameValuePairs.add(new BasicNameValuePair("uid_from_android", strs_Data[0]));
                break;
            case SERVER_REQ_GETSTUDYINFO:
                str_ServerURL = "http://your_domain_name/StudyTracker/str_GETSTUDYINFO.jsp";
                nameValuePairs.add(new BasicNameValuePair("uid_from_android", strs_Data[0]));
                break;
            case SERVER_REQ_GETSTUDIEDLIST:
                str_ServerURL = "http://your_domain_name/StudyTracker/str_GETSTUDIEDLIST.jsp";
                nameValuePairs.add(new BasicNameValuePair("uid_from_android", strs_Data[0]));
                break;
        }

        lst_DBResult = str_ServerResultParse2(str_RequestContactToServer(str_ServerURL, nameValuePairs));
        return lst_DBResult;
    }

    //- 서버 통신 -//
    private String str_RequestContactToServer(String url, ArrayList<NameValuePair> nameValuePairs) {
        String entity = null;

        try {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);

            HttpClient http = new DefaultHttpClient();
            HttpParams params = http.getParams();
            HttpConnectionParams.setConnectionTimeout(params, 5000);
            HttpConnectionParams.setSoTimeout(params, 5000);

            HttpPost httpPost = new HttpPost(url);
            UrlEncodedFormEntity entityRequest = new UrlEncodedFormEntity(nameValuePairs, "UTF-8");

            httpPost.setEntity(entityRequest);

            HttpResponse responsePost = http.execute(httpPost);
            HttpEntity resEntity = responsePost.getEntity();

            entity = EntityUtils.toString(resEntity);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            return entity;
        }
    }

    //- 서버 통신결과 파싱 > T/F 반환 -//
    private boolean b_ServerResultParse(String str_Entity) {
        String str_ParsedData = "false";
        boolean b_DBResult = false;

        try {
            if (str_Entity != null) {
                JSONObject json_Object = new JSONObject(str_Entity);
                JSONArray json_Array = json_Object.getJSONArray("finished");
                for (int i = 0; i < json_Array.length(); i++) {
                    json_Object = json_Array.getJSONObject(i);
                    str_ParsedData = json_Object.getString("result");
                }
            } else {
                str_ParsedData = "false";
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        StackTraceElement[] ste = new Throwable().getStackTrace();
        for (int i = ste.length - 1; i > 0; i--) {
            if (str_ParsedData.equals("true")) {
                b_DBResult = true;
            } else if (str_ParsedData.equals("false")) {
                b_DBResult = false;
            }
        }
        return b_DBResult;
    }

    //- 서버 통신결과 파싱 > String List 반환 -//
    private List str_ServerResultParse(String str_Entity) {
        List lst_DBResult = new ArrayList();
        int i_ListCounter = 0;

        try {
            JSONObject json_Object = new JSONObject(str_Entity);
            JSONArray json_Array = json_Object.getJSONArray("finished");
            for (int i = 0; i < json_Array.length(); i++) {
                json_Object = json_Array.getJSONObject(i);

                for (int j = 0; !json_Object.getString(Integer.toString(j)).equals(""); j++) {
                    lst_DBResult.add(i_ListCounter, json_Object.getString(Integer.toString(j)));
                    i_ListCounter++;
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return lst_DBResult;
    }

    private ArrayList<ArrayList<String>> str_ServerResultParse2(String str_Entity) {
        ArrayList<ArrayList<String>> lst_DBResult = new ArrayList<ArrayList<String>>();
        ArrayList<String> lst_temp = new ArrayList();

        try {
            JSONObject json_Object = new JSONObject(str_Entity);
            JSONArray json_Array = json_Object.getJSONArray("finished");
            int size = json_Array.length();

            for (int i = 0; i < size; i++) {
                int size2 = json_Array.getJSONObject(i).length();
                for (int j = 0; j < size2; j++) {
                    lst_temp.add(j, json_Array.getJSONObject(i).getString(Integer.toString(j)));
                }
                lst_DBResult.add(lst_temp);
            }
        } catch (Exception e) {
            Log.e("SERVER", e.getMessage());
        }

        return lst_DBResult;
    }

    protected void PhotoUpload(String FilePath) {
        try {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);

            HttpClient httpClient = new DefaultHttpClient();
            HttpParams params = httpClient.getParams();
            HttpConnectionParams.setConnectionTimeout(params, 5000);
            HttpConnectionParams.setSoTimeout(params, 5000);

            HttpPost httpPost = new HttpPost("http://racersyun.dlinkddns.com:4116/StudyTracker/IMGUPLOAD.jsp");
            MultipartEntityBuilder builder = MultipartEntityBuilder.create().setCharset(Charset.forName("UTF-8")).setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
            builder.addPart("fileData", new FileBody(new File(FilePath)));

            httpPost.setEntity(builder.build());
            httpClient.execute(httpPost);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    protected void PhotoDownload(String uploadedFileName) {
        String ServerUrl = "http://racersyun.dlinkddns.com:4116/StudyTracker/IMGDOWNLOAD.jsp";
        String FilePath = "/sdcard/Android/data/StudyTrackerData/IMG/";
        String FileName = "cache.jpg";

        URL imgurl;
        int Read;

        try {
            String data = URLEncoder.encode("fileName", "UTF-8") + "=" + URLEncoder.encode(uploadedFileName, "UTF-8");

            imgurl = new URL(ServerUrl);
            HttpURLConnection conn = (HttpURLConnection) imgurl.openConnection();
            conn.setDoOutput(true);

            OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
            wr.write(data);
            wr.flush();

            conn.connect();
            int len = conn.getContentLength();

            byte[] tmpByte = new byte[len];

            InputStream is = conn.getInputStream();

            File pathchk = new File(FilePath);
            if (!pathchk.exists()) {
                pathchk.mkdirs();
            }

            File file = new File(FilePath + FileName);
            FileOutputStream fos = new FileOutputStream(file);

            for (; ; ) {
                Read = is.read(tmpByte);

                if (Read <= 0) {
                    break;
                }
                fos.write(tmpByte, 0, Read);
            }

            is.close();
            fos.close();
            conn.disconnect();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    protected void XMLDownload(String FilePath, String FileName) {
        String ServerUrl = null;

        switch (FileName) {
            case "haarcascade_frontalface_alt.xml":
                ServerUrl = "http://your_domain_name/StudyTracker/XMLFACEDOWNLOAD.jsp";
                break;
            case "haarcascade_eye_tree_eyeglasses.xml":
                ServerUrl = "http://your_domain_name/StudyTracker/XMLEYEDOWNLOAD.jsp";
                break;
        }

        URL imgurl;
        int Read;

        try {
            imgurl = new URL(ServerUrl);
            HttpURLConnection conn = (HttpURLConnection) imgurl.openConnection();
            conn.connect();
            int len = conn.getContentLength();

            byte[] tmpByte = new byte[len];

            InputStream is = conn.getInputStream();

            File pathchk = new File(FilePath);
            if (!pathchk.exists()) {
                pathchk.mkdirs();
            }

            File file = new File(FilePath + FileName);
            FileOutputStream fos = new FileOutputStream(file);

            for (; ; ) {
                Read = is.read(tmpByte);

                if (Read <= 0) {
                    break;
                }
                fos.write(tmpByte, 0, Read);
            }

            is.close();
            fos.close();
            conn.disconnect();

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
