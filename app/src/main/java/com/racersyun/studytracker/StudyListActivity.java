package com.racersyun.studytracker;

import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;

public class StudyListActivity extends AppCompatActivity {
    ListView listview;
    ArrayAdapter<String> adapter;
    NetworkServiceClass nsc = new NetworkServiceClass();
    String str_UserID;
    TimeServiceClass tsc = new TimeServiceClass();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_study_list);
        setTitle("학습 내역 확인");

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Intent intent = getIntent();
        str_UserID = intent.getStringExtra("it_UserID");

        listview = (ListView) findViewById(R.id.lv_StudiedListView);
        adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1);
        listview.setAdapter(adapter);

        StudyListView();
    }

    private void StudyListView() {
        ArrayList<ArrayList<String>> list = new ArrayList<ArrayList<String>>();

        list = nsc.str_SetDataForServer(nsc.SERVER_REQ_GETSTUDIEDLIST, str_UserID);

        if (!list.isEmpty()) {
            adapter.clear();
            int count = 0;
            for (int i = 0; i < list.get(0).size() / 5; i++) {
                String tmp = "";
                for (int j = 0; j < 5; j++) {
                    switch (j) {
                        case 0:
                            tmp += list.get(0).get(count).toString();
                            break;
                        case 1:
                            tmp += "\n시작시간  :  ";
                            tmp += list.get(0).get(count).toString();
                            break;
                        case 2:
                            tmp += "\n진행시간  :  ";
                            tmp += tsc.GetConvertedTimeForParent(list.get(0).get(count).toString());
                            break;
                        case 3:
                            tmp += "\n집중시간  :  ";
                            tmp += tsc.GetConvertedTimeForParent(list.get(0).get(count).toString());
                            break;
                        case 4:
                            tmp += "\n  집중도    :  ";
                            tmp += list.get(0).get(count).toString();
                            break;
                    }
                    if (j == 4) tmp += " %";
                    count++;
                }
                adapter.add(tmp);
            }
            adapter.notifyDataSetChanged();
        } else {
            adapter.clear();
            adapter.add("공부 내역이 없어요!");
            adapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(this, MainActivity.class));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}

