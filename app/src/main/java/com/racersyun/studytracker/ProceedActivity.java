package com.racersyun.studytracker;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.ColorStateList;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.media.AudioManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.Settings;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import androidx.appcompat.app.AlertDialog;

import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.macroyau.blue2serial.BluetoothDeviceListDialog;
import com.macroyau.blue2serial.BluetoothSerial;
import com.macroyau.blue2serial.BluetoothSerialListener;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;

/**
 * Created by racer on 2016-07-29.
 */
public class ProceedActivity extends Activity implements BluetoothSerialListener, BluetoothDeviceListDialog.OnDeviceSelectedListener {
    private static final int DIALOG_PROGRESS_ID = 1;
    private static final int REQUEST_ENABLE_BLUETOOTH = 1;

    private FloatingActionButton fab_StudyPauseFab, fab_StudyStopFab;
    private TextView tv_SubjectNameView, tv_StartTimeView, tv_TotalTimeView, tv_FocusTimeView, tv_BTStatusView;
    private LinearLayout ll_CushionLayoutView;

    final int MSG_START_TIMER = 0;
    final int MSG_STOP_TIMER = 1;
    final int MSG_RESUME_TIMER = 3;

    private boolean b_isBtnResumeMode = false;
    private boolean b_isCushionResumeMode = false;
    private boolean CRLF = false;
    private String str_UserID = "";
    private String SubjectNametmp = "", StartTimetmp = "", TotalTimetmp = "", FilePathtmp = "", FileNametmp = "";
    private String str_BTDeviceReceivedRawValue;
    private int i_StudyStatusUploadCounter = 21;
    private int i_CushionStatusUpdateCounter = 0;
    private int i_CurrentDeviceBrightness = 250;
    private int i_CurrentAutoBrightness = 0;
    private int i_CurrentRingerMode;
    private int i_BTDeviceLoopCounter = 0;
    private int[] i_BTDeviceReceivedValue = new int[5];
    private int[] i_BTDeviceAverage = new int[5];

    private FrameLayout fl_CameraLayoutView;
    private ImageView iv_CameraView = null;

    private CameraServiceClass csc;
    private CushionViewClass cvc;
    private BluetoothSerial BTSerial;
    private NetworkServiceClass nsc = new NetworkServiceClass();
    private TimeServiceClass tsc = new TimeServiceClass();
    private TimeServiceClass tsc_TotalTimeObj, tsc_FocusTimeObj;

    private Handler mHandler;
    private FaceDetectCounterEnableThread fdc;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        SystemStudyMode(true);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        setContentView(R.layout.activity_proceed);

        tv_SubjectNameView = (TextView) findViewById(R.id.tv_SubjectNameView);
        tv_StartTimeView = (TextView) findViewById(R.id.tv_StartTimeView);
        tv_TotalTimeView = (TextView) findViewById(R.id.tv_TotalTimeView);
        tv_FocusTimeView = (TextView) findViewById(R.id.tv_FocusTimeView);
        tv_BTStatusView = (TextView) findViewById(R.id.tv_BTStatusView);
        fab_StudyPauseFab = (FloatingActionButton) findViewById(R.id.fab_StudyPauseFab);
        fab_StudyStopFab = (FloatingActionButton) findViewById(R.id.fab_StudyStopFab);
        ll_CushionLayoutView = (LinearLayout) findViewById(R.id.ll_CushionLayoutView);

        Intent intent = getIntent();
        tv_SubjectNameView.setText(intent.getStringExtra("it_SubjectName"));
        tv_StartTimeView.setText(intent.getStringExtra("it_StudyStartTime"));
        str_UserID = intent.getStringExtra("it_UserID");

        if (str_UserID.equals("")) {
            str_UserID = "Guest";
        }

        tsc_TotalTimeObj = new TimeServiceClass(tv_TotalTimeView);
        tsc_TotalTimeObj.WatchMsgReceiver(MSG_START_TIMER);
        tsc_FocusTimeObj = new TimeServiceClass(tv_FocusTimeView);
        tsc_FocusTimeObj.WatchMsgReceiver(MSG_START_TIMER);

        cvc = new CushionViewClass(this, 290, 50, 500, 500);
        ll_CushionLayoutView.addView(cvc);

        BTSerial = new BluetoothSerial(this, this);

        mHandler = new Handler();
        fdc = new FaceDetectCounterEnableThread();
        fdc.start();

        StudyProceedViewStart();
    }

    @Override
    protected void onStart() {
        super.onStart();
        BTSerial.setup();
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (BTSerial.checkBluetooth() && BTSerial.isBluetoothEnabled()) {
            if (!BTSerial.isConnected()) {
                BTSerial.start();
                BTDeviceAutoConnect();
            }
        }
        BTDeviceStatusUpdate();
    }

    @Override
    protected void onPause() {
        if (csc != null)
            csc.onPause();
        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
        BTSerial.stop();
    }

    @Override
    public void onBackPressed() {
        Toast.makeText(this, "앱을 종료하려면 먼저 공부를 중지하십시오", Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case REQUEST_ENABLE_BLUETOOTH:
                if (resultCode == Activity.RESULT_OK) {
                    BTSerial.setup();
                }
                break;
        }
    }

    public void onClickFab(View v) {
        switch (v.getId()) {
            case R.id.fab_StudyPauseFab:
                if (b_isBtnResumeMode) {   // 재개 버튼 클릭
                    fab_StudyStopFab.setVisibility(View.INVISIBLE);
                    fab_StudyPauseFab.setBackgroundTintList(new ColorStateList(new int[][]{new int[0]}, new int[]{Color.parseColor("#1c89da")}));
                    fab_StudyPauseFab.setImageResource(R.drawable.ic_pause);
                    b_isBtnResumeMode = false;

                    tsc_TotalTimeObj.WatchMsgReceiver(MSG_RESUME_TIMER);
                } else {     // 일시정지 버튼 클릭
                    fab_StudyStopFab.setVisibility(View.VISIBLE);
                    fab_StudyPauseFab.setBackgroundTintList(new ColorStateList(new int[][]{new int[0]}, new int[]{Color.parseColor("#98cb33")}));
                    fab_StudyPauseFab.setImageResource(R.drawable.ic_start);
                    b_isBtnResumeMode = true;
                    i_EyeTrackingPauseCounter = 6;

                    tsc_TotalTimeObj.WatchMsgReceiver(MSG_STOP_TIMER);
                }
                break;
            case R.id.fab_StudyStopFab:
                SystemStudyMode(false);
                csc.onStop();
                csc = null;

                if (i_BTDeviceLoopCounter > 0) {
                    for (int i = 0; i < 5; i++) {
                        i_BTDeviceAverage[i] = (int) (i_BTDeviceAverage[i] / (double) i_BTDeviceLoopCounter);
                    }
                }

                Intent intent = new Intent(ProceedActivity.this, FinishActivity.class);
                intent.putExtra("it_SubjectName", tv_SubjectNameView.getText().toString());
                intent.putExtra("it_StudyStartTime", tv_StartTimeView.getText().toString());
                intent.putExtra("it_StudyTotalTime", tv_TotalTimeView.getText().toString());
                intent.putExtra("it_StudyFocusTime", tv_FocusTimeView.getText().toString());
                intent.putExtra("it_StudyFinishTime", tsc.GetTimeDateForDisplay());
                intent.putExtra("it_UserID", str_UserID);
                intent.putExtra("it_BTAverage", i_BTDeviceAverage);
                startActivity(intent);
                finish();
                overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
                break;
        }
    }

    private void SystemStudyMode(boolean studyenable) {
        AudioManager audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        int ringermode, brightness, autobrightness;

        if (studyenable) {
            try {
                i_CurrentRingerMode = audioManager.getRingerMode();
                i_CurrentDeviceBrightness = Settings.System.getInt(getContentResolver(), Settings.System.SCREEN_BRIGHTNESS);
                i_CurrentAutoBrightness = Settings.System.getInt(getContentResolver(), Settings.System.SCREEN_BRIGHTNESS_MODE);
            } catch (Exception e) {
                Log.e("StudyModeErr", e.getMessage());
            } finally {
                ringermode = AudioManager.RINGER_MODE_SILENT;
                brightness = 10;
                autobrightness = 0;
            }
        } else {
            ringermode = i_CurrentRingerMode;
            brightness = i_CurrentDeviceBrightness;
            autobrightness = i_CurrentAutoBrightness;
        }

        try {
            audioManager.setRingerMode(ringermode);

            if (Settings.System.getInt(getContentResolver(), Settings.System.SCREEN_BRIGHTNESS_MODE) == 1) {
                Settings.System.putInt(getContentResolver(), Settings.System.SCREEN_BRIGHTNESS_MODE, autobrightness);
            }

            Settings.System.putInt(getContentResolver(), "screen_brightness", brightness);
            WindowManager.LayoutParams params = getWindow().getAttributes();
            params.screenBrightness = brightness;
            getWindow().setAttributes(params);
        } catch (Exception e) {
            Log.e("Brightness", e.getMessage());
        }
    }

    private void StudyProceedViewStart() {
        int i_CamPreviewSizeWidth = 640;
        int i_CamPreviewSizeHeight = 480;

        iv_CameraView = new ImageView(this);
        SurfaceView camView = new SurfaceView(this);

        SurfaceHolder camHolder = camView.getHolder();
        csc = new CameraServiceClass(this, i_CamPreviewSizeWidth, i_CamPreviewSizeHeight, iv_CameraView, CameraServiceClass.CSC_MODE_PROCESS);

        camHolder.addCallback(csc);
        camHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);

        fl_CameraLayoutView = (FrameLayout) findViewById(R.id.fl_CameraLayoutView);
        fl_CameraLayoutView.addView(camView);
        fl_CameraLayoutView.addView(iv_CameraView);
    }

    int i_EyeTrackingPauseCounter = 0;

    class FaceDetectCounterEnableThread extends Thread {
        @Override
        public void run() {
            super.run();
            while (true) {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                mHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        int i_EyeTrackingSuccessStatus = 1;
                        try {
                            i_EyeTrackingSuccessStatus = csc.i_EyeTrackingSuccessStatus;
                        } catch (Exception e) {
                        }
                        if (i_EyeTrackingSuccessStatus == 0 || b_isBtnResumeMode || !b_isCushionResumeMode) {
                            if (tsc_FocusTimeObj.WatchMsgReturner() && i_EyeTrackingPauseCounter > 5) {
                                tsc_FocusTimeObj.WatchMsgReceiver(MSG_STOP_TIMER);
                            }
                            i_EyeTrackingPauseCounter++;
                            Log.e("FACEDELAY", Integer.toString(i_EyeTrackingPauseCounter));
                        } else {
                            if (tsc_FocusTimeObj.WatchMsgReturner() == false) {
                                tsc_FocusTimeObj.WatchMsgReceiver(MSG_RESUME_TIMER);
                                i_EyeTrackingPauseCounter = 0;
                            } else {
                                if (i_StudyStatusUploadCounter < 20) {
                                    i_StudyStatusUploadCounter++;
                                } else {
                                    StudyFaceCapture();
                                    i_StudyStatusUploadCounter = 0;
                                }
                            }
                        }
                        CushionStatusUpdate csu = new CushionStatusUpdate();
                        csu.execute();
                    }
                });
            }
        }

        private void StudyFaceCapture() {
            fl_CameraLayoutView.destroyDrawingCache();
            fl_CameraLayoutView.buildDrawingCache();

            Bitmap captureView = fl_CameraLayoutView.getDrawingCache();
            FileOutputStream fos;
            String str_IMGFileFullPath = getApplicationContext().getExternalFilesDir(null).getAbsolutePath() + "/capture";

            String str_CapturedIMGName = str_UserID + tsc.GetTimeForImage() + ".jpg";
            File f_FolderPathExistChk = new File(str_IMGFileFullPath);
            File f_UploadIMGFile = new File(str_IMGFileFullPath + "/" + str_CapturedIMGName);

            if (!f_FolderPathExistChk.exists()) {
                f_FolderPathExistChk.mkdirs();
            }

            try {
                fos = new FileOutputStream(f_UploadIMGFile);
                captureView.compress(Bitmap.CompressFormat.JPEG, 100, fos);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }

            Log.e("CAPTURE", str_CapturedIMGName);

            FilePathtmp = str_IMGFileFullPath;
            FileNametmp = str_CapturedIMGName;

            if (!str_UserID.equals("Guest")) {
                showDialog(1);
            }
        }
    }

    private class ProgressThread extends Thread {
        Handler mHandler;

        ProgressThread(Handler h) {
            mHandler = h;
        }

        public void run() {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    SubjectNametmp = tv_SubjectNameView.getText().toString();
                    StartTimetmp = tv_StartTimeView.getText().toString();
                    TotalTimetmp = tv_TotalTimeView.getText().toString();
                }
            });

            nsc.PhotoUpload(FilePathtmp + "/" + FileNametmp);
            nsc.b_SetDataForServer(nsc.SERVER_REQ_UPDATESTUDYINFO, str_UserID, SubjectNametmp
                    , tsc.GetConvertedTimeForServer(StartTimetmp), TotalTimetmp, FileNametmp);
            Message msg = mHandler.obtainMessage();
            mHandler.sendMessage(msg);
        }
    }

    final Handler handler = new Handler() {
        public void handleMessage(Message msg) {
            dismissDialog(DIALOG_PROGRESS_ID);
            removeDialog(DIALOG_PROGRESS_ID);
        }
    };

    protected Dialog onCreateDialog(int id) {
        switch (id) {
            case DIALOG_PROGRESS_ID:
                ProgressDialog progressDialog = new ProgressDialog(ProceedActivity.this);
                progressDialog.setMessage("업로드중...");
                new ProceedActivity.ProgressThread(handler).start();
                return progressDialog;
        }
        return null;
    }

    private class CushionStatusUpdate extends AsyncTask<Void, Void, Boolean> {
        @Override
        protected Boolean doInBackground(Void... params) {
            if (i_CushionStatusUpdateCounter++ > 10) {
                try {
                    str_BTDeviceReceivedRawValue = "";
                    SendDataToBTDevice("q");
                } catch (Exception e) {
                }
                i_CushionStatusUpdateCounter = 0;
            }
            Log.e("CAMERAMOVE", "CUSHION" + Integer.toString(i_CushionStatusUpdateCounter));
            return true;
        }
    }

    private void BTDeviceAutoConnect() {
        SharedPreferences prefs = getSharedPreferences("sp_AutoBluetooth", MODE_PRIVATE);
        String autoPairedDeviceAddr = prefs.getString("spref_PairedDeviceAddr", "");
        Log.e("BTADDR", autoPairedDeviceAddr);
        if (!autoPairedDeviceAddr.equals("")) {
            BTSerial.connect(autoPairedDeviceAddr);
            BTDeviceStatusUpdate();
        }
    }

    private void BTDeviceStatusUpdate() {
        final int i_CurrentState;
        if (BTSerial != null) {
            i_CurrentState = BTSerial.getState();
        } else {
            i_CurrentState = BluetoothSerial.STATE_DISCONNECTED;
        }

        String str_CurrentStateMsg = "블루투스가 연결되지 않았습니다";
        switch (i_CurrentState) {
            case BluetoothSerial.STATE_CONNECTED:
                str_CurrentStateMsg = BTSerial.getConnectedDeviceName() + " 연결됨";
                break;
            default:
                break;
        }
        tv_BTStatusView.setText(str_CurrentStateMsg);
    }

    private void SendDataToBTDevice(String data) {
        if (data.length() > 0) {
            BTSerial.write(data, CRLF);
        }
    }

    private void ParseBTDataToInt(String data) {
        str_BTDeviceReceivedRawValue = "";
        for (int i = 0; i < 5; i++) {
            int index = data.indexOf("#" + Integer.toString(i) + "#");
            String temp = data.substring(index + 3, data.substring(index + 3).indexOf("#") + index + 3);

            if (!b_IsInteger(temp)) {
                temp = "0";
            }
            Log.e("BTPARSE", temp);
            i_BTDeviceReceivedValue[i] = Integer.parseInt(temp);
            str_BTDeviceReceivedRawValue += Integer.toString(i_BTDeviceReceivedValue[i]) + " ";
        }
        ParseBTDataToPercent();
    }

    private boolean b_IsInteger(String data) {
        try {
            Integer.parseInt(data);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    private void ParseBTDataToPercent() {
        int i_SumOfDeviceValue = 1;

        str_BTDeviceReceivedRawValue = "";
        for (int i = 0; i < 5; i++) {
            if (i_BTDeviceReceivedValue[i] < 15) {
                i_BTDeviceReceivedValue[i] = 0;
            }
            i_SumOfDeviceValue += i_BTDeviceReceivedValue[i];
        }

        if (i_SumOfDeviceValue > 80) {
            for (int i = 0; i < 5; i++) {
                i_BTDeviceReceivedValue[i] = (int) ((double) i_BTDeviceReceivedValue[i] / i_SumOfDeviceValue * 100);
                i_BTDeviceAverage[i] += i_BTDeviceReceivedValue[i];
            }
            i_BTDeviceLoopCounter++;
            b_isCushionResumeMode = true;
        } else {
            b_isCushionResumeMode = false;
        }

        cvc.setCushionData(i_BTDeviceReceivedValue);
        ll_CushionLayoutView.removeAllViews();
        ll_CushionLayoutView.addView(cvc);
    }

    @Override
    public void onBluetoothNotSupported() {
        new AlertDialog.Builder(this)
                .setMessage("블루투스가 지원되지 않는 기기입니다")
                .setPositiveButton("확인", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                })
                .setCancelable(false)
                .show();
    }

    @Override
    public void onBluetoothDisabled() {
        Intent enableBluetooth = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
        startActivityForResult(enableBluetooth, REQUEST_ENABLE_BLUETOOTH);
    }

    @Override
    public void onBluetoothDeviceDisconnected() {
        invalidateOptionsMenu();
        BTDeviceStatusUpdate();
    }

    @Override
    public void onConnectingBluetoothDevice() {
        BTDeviceStatusUpdate();
    }

    @Override
    public void onBluetoothDeviceConnected(String name, String address) {
        invalidateOptionsMenu();
        BTDeviceStatusUpdate();
        i_CushionStatusUpdateCounter = 11;
    }

    @Override
    public void onBluetoothSerialRead(String message) {
        str_BTDeviceReceivedRawValue += getString(R.string.terminal_message_template, message);
        if (str_BTDeviceReceivedRawValue.contains("#!")) {
            ParseBTDataToInt(str_BTDeviceReceivedRawValue);
        }
    }

    @Override
    public void onBluetoothSerialWrite(String message) {
    }

    @Override
    public void onBluetoothDeviceSelected(BluetoothDevice device) {
    }
}