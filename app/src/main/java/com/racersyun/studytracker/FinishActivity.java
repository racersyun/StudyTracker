package com.racersyun.studytracker;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by racer on 2016-07-29.
 */
public class FinishActivity extends Activity {
    private TextView tv_SubjectNameView, tv_StartTimeView, tv_TotalTimeView, tv_FocusTimeView, tv_FinishTimeView, tv_FocusRatioView;
    private String str_UserID = "";
    private String str_FocusRate;
    private LinearLayout ll_CushionLayoutView;

    private NetworkServiceClass nsc = new NetworkServiceClass();
    private TimeServiceClass tsc = new TimeServiceClass();
    private CushionViewClass cvc;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_finish);

        tv_SubjectNameView = (TextView) findViewById(R.id.tv_SubjectNameView);
        tv_StartTimeView = (TextView) findViewById(R.id.tv_StartTimeView);
        tv_TotalTimeView = (TextView) findViewById(R.id.tv_TotalTimeView);
        tv_FocusTimeView = (TextView) findViewById(R.id.tv_FocusTimeView);
        tv_FinishTimeView = (TextView) findViewById(R.id.tv_FinishTimeView);
        tv_FocusRatioView = (TextView) findViewById(R.id.tv_FocusRatioView);

        Intent intent = getIntent();
        tv_SubjectNameView.setText(intent.getStringExtra("it_SubjectName"));
        tv_StartTimeView.setText(intent.getStringExtra("it_StudyStartTime"));
        tv_TotalTimeView.setText(intent.getStringExtra("it_StudyTotalTime").substring(0, 12));
        tv_FocusTimeView.setText(intent.getStringExtra("it_StudyFocusTime").substring(0, 12));
        tv_FinishTimeView.setText(intent.getStringExtra("it_StudyFinishTime"));
        str_UserID = intent.getStringExtra("it_UserID");
        int[] i_BTDeviceAverage = intent.getIntArrayExtra("it_BTAverage");
        str_FocusRate = tsc.GetFocusRate(tv_TotalTimeView.getText().toString(), tv_FocusTimeView.getText().toString());
        tv_FocusRatioView.setText(str_FocusRate + "%");

        ll_CushionLayoutView = (LinearLayout) findViewById(R.id.ll_CushionLayoutView);

        int[] i_StudiedTime = tsc.GetConvertedTimeForInt(tv_TotalTimeView.getText().toString());

        SharedPreferences prefs = getSharedPreferences("sp_TodayStudyTime" + str_UserID + tsc.GetDateForSharedPreferences(), MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        int i_studiedHour = prefs.getInt("spref_studyHour", 0);
        int i_studiedMinute = prefs.getInt("spref_studyMinute", 0);
        editor.putInt("spref_studyHour", i_studiedHour + i_StudiedTime[0]);
        editor.putInt("spref_studyMinute", i_studiedMinute + i_StudiedTime[1]);
        editor.commit();

        cvc = new CushionViewClass(this, 290, 0, 500, 500);
        cvc.setCushionData(i_BTDeviceAverage);
        ll_CushionLayoutView.addView(cvc);

        if (str_UserID.equals("Guest")) {
            Toast toast_Msg = Toast.makeText(FinishActivity.this, "로그인이 되어있지 않아 결과 정보가 업로드되지 않습니다", Toast.LENGTH_SHORT);
            toast_Msg.show();
        } else {
            showDialog(1);
        }
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(FinishActivity.this, MainActivity.class);
        startActivity(intent);
        finish();
    }

    public void OnClick(View v) {
        switch (v.getId()) {
            case R.id.btn_StudyFinishBtn:
                Intent intent = new Intent(FinishActivity.this, MainActivity.class);
                startActivity(intent);
                finish();
                break;
        }
    }

    private static final int DIALOG_PROGRESS_ID = 1;

    private class ProgressThread extends Thread {
        Handler mHandler;

        ProgressThread(Handler h) {
            mHandler = h;
        }

        public void run() {
            nsc.b_SetDataForServer(nsc.SERVER_REQ_ADDSTUDY, str_UserID, tv_SubjectNameView.getText().toString()
                    , tsc.GetConvertedTimeForServer(tv_StartTimeView.getText().toString())
                    , tsc.GetConvertedTimeForServer(tv_FinishTimeView.getText().toString())
                    , tv_TotalTimeView.getText().toString()
                    , tv_FocusTimeView.getText().toString(), str_FocusRate);
            Message msg = mHandler.obtainMessage();
            mHandler.sendMessage(msg);
        }
    }

    final Handler handler = new Handler() {
        public void handleMessage(Message msg) {
            dismissDialog(DIALOG_PROGRESS_ID);
            removeDialog(DIALOG_PROGRESS_ID);
        }
    };

    protected Dialog onCreateDialog(int id) {
        switch (id) {
            case DIALOG_PROGRESS_ID:
                ProgressDialog progressDialog = new ProgressDialog(FinishActivity.this);
                progressDialog.setMessage("서버에 업로드 중...");
                new FinishActivity.ProgressThread(handler).start();
                return progressDialog;
        }
        return null;
    }
}
