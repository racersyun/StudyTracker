package com.racersyun.studytracker;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.ImageFormat;
import android.hardware.Camera;
import android.os.AsyncTask;
import android.util.Log;
import android.view.SurfaceHolder;
import android.widget.ImageView;

import org.opencv.android.Utils;
import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.Point;
import org.opencv.imgproc.Imgproc;

import java.io.IOException;

/**
 * Created by racer on 2016-08-21.
 */
public class CameraServiceClass implements SurfaceHolder.Callback, Camera.PreviewCallback {
    public native int EyeTracking(int width, int height, long matAddrInput, long matAddrResult, String faceXmlPath, String eyeXmlPath);

    static {
        System.loadLibrary("opencv_java4");
        System.loadLibrary("eye-tracking");
    }

    private Camera mCamera = null;
    private ImageView iv_CameraView = null;
    private Bitmap bitmap = null;
    private int imageFormat;
    private int PreviewSizeWidth;
    private int PreviewSizeHeight;
    private int i_CameraRunningMode;
    private boolean bProcessing = false;
    protected int i_EyeTrackingSuccessStatus = 1;

    final static int CSC_MODE_MAIN = 1;
    final static int CSC_MODE_PROCESS = 2;
    final static int CSC_MODE_SETTING = 3;

    FaceDetectForMain fd_Main;
    FaceDetectForProcess fd_Process;
    FaceDetectForSetting fd_Setting;

    private Mat inputImg, outputImg;

    private final Context mContext;

    public CameraServiceClass(Context context, int PreviewlayoutWidth, int PreviewlayoutHeight, ImageView CameraPreview, int i_CameraRunningMode) {
        mContext = context;

        PreviewSizeWidth = PreviewlayoutWidth;
        PreviewSizeHeight = PreviewlayoutHeight;
        iv_CameraView = CameraPreview;
        bitmap = Bitmap.createBitmap(PreviewSizeWidth, PreviewSizeHeight, Bitmap.Config.ARGB_8888);
        this.i_CameraRunningMode = i_CameraRunningMode;

        inputImg = new Mat(PreviewlayoutWidth, PreviewlayoutHeight, CvType.CV_8UC3);
        outputImg = new Mat(PreviewlayoutWidth, PreviewlayoutHeight, CvType.CV_8UC3);
    }

    @Override
    public void onPreviewFrame(byte[] arg0, Camera arg1) {
        if ((imageFormat == ImageFormat.NV21) && (arg0 != null) && (!bProcessing)) {
            inputImg = ByteIMGToMat(arg0);
            switch (i_CameraRunningMode) {
                case CSC_MODE_MAIN:
                    if (fd_Main != null) {
                        if (fd_Main.getStatus() != AsyncTask.Status.RUNNING) {
                            fd_Main = new FaceDetectForMain();
                            fd_Main.execute();
                        }
                    } else {
                        fd_Main = new FaceDetectForMain();
                        fd_Main.execute();
                    }
                    break;
                case CSC_MODE_PROCESS:
                    if (fd_Process != null) {
                        if (fd_Process.getStatus() != AsyncTask.Status.RUNNING) {
                            fd_Process = new FaceDetectForProcess();
                            fd_Process.execute();
                        }
                    } else {
                        fd_Process = new FaceDetectForProcess();
                        fd_Process.execute();
                    }
                    break;
                case CSC_MODE_SETTING:
                    if (fd_Setting != null) {
                        if (fd_Setting.getStatus() != AsyncTask.Status.RUNNING) {
                            fd_Setting = new FaceDetectForSetting();
                            fd_Setting.execute();
                        }
                    } else {
                        fd_Setting = new FaceDetectForSetting();
                        fd_Setting.execute();
                    }
                    break;
            }
        }
    }

    public void onPause() {
        if (mCamera != null) {
            mCamera.stopPreview();
        }
    }

    public void onStop() {
        if (mCamera != null) {
            mCamera.setPreviewCallback(null);
            mCamera.stopPreview();
            mCamera.release();
            mCamera = null;
        }
    }

    @Override
    public void surfaceChanged(SurfaceHolder arg0, int arg1, int arg2, int arg3) {
        Camera.Parameters parameters;

        parameters = mCamera.getParameters();
        // Set the camera preview size
        parameters.setPreviewSize(PreviewSizeWidth, PreviewSizeHeight);
        imageFormat = parameters.getPreviewFormat();
        mCamera.setParameters(parameters);
        mCamera.startPreview();
    }

    @Override
    public void surfaceCreated(SurfaceHolder arg0) {
        // TODO : 카메라 권한 획득 필요
        mCamera = Camera.open(Camera.CameraInfo.CAMERA_FACING_FRONT);

        try {
            mCamera.setPreviewDisplay(arg0);
            mCamera.setPreviewCallback(this);
        } catch (IOException e) {
            mCamera.release();
            mCamera = null;
        }
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder arg0) {
        if (mCamera != null) {
            mCamera.setPreviewCallback(null);
            mCamera.stopPreview();
            mCamera.release();
            mCamera = null;
        }
    }

    private Mat FaceDetect() {
        final String faceXmlFileName = "haarcascade_frontalface_alt.xml";
        final String eyeXmlFileName = "haarcascade_eye_tree_eyeglasses.xml";
        final String faceXmlFilePath = mContext.getFilesDir().getAbsolutePath() + "/" + faceXmlFileName;
        final String eyeXmlFilePath = mContext.getFilesDir().getAbsolutePath() + "/" + eyeXmlFileName;
        bProcessing = true;
        i_EyeTrackingSuccessStatus = EyeTracking(PreviewSizeWidth, PreviewSizeHeight, inputImg.getNativeObjAddr(), outputImg.getNativeObjAddr(), faceXmlFilePath, eyeXmlFilePath);
        bProcessing = false;
        return i_EyeTrackingSuccessStatus == -1 ? inputImg : outputImg;
    }

    private Mat ByteIMGToMat(byte[] srcImg) {
        Mat mYuvImage = new Mat(PreviewSizeHeight + PreviewSizeHeight / 2, PreviewSizeWidth, CvType.CV_8UC1);
        mYuvImage.put(0, 0, srcImg);
        Mat mRgbaImage = new Mat();
        Imgproc.cvtColor(mYuvImage, mRgbaImage, Imgproc.COLOR_YUV2RGB_NV21, 4);
        mRgbaImage = MatIMGRotate(mRgbaImage);
        return mRgbaImage;
    }

    private Mat MatIMGRotate(Mat srcImg) {
        Point center = new Point(PreviewSizeWidth / 2.0, PreviewSizeHeight / 2.0);
        Mat rotateAngleImage = Imgproc.getRotationMatrix2D(center, 0.0, 1.5);
        Mat rotatedImage = new Mat();
        Imgproc.warpAffine(srcImg, rotatedImage, rotateAngleImage, rotatedImage.size());
        Core.flip(rotatedImage, rotatedImage, 1);
        return rotatedImage;
    }

    private class FaceDetectForMain extends AsyncTask<Void, Void, Mat> {
        @Override
        protected Mat doInBackground(Void... params) {
            outputImg = FaceDetect();
            return outputImg;
        }

        @Override
        protected void onPostExecute(Mat result) {
            super.onPostExecute(result);
            try {
                Utils.matToBitmap(result, bitmap);
                iv_CameraView.setImageBitmap(bitmap);
            } catch (Exception e) {
                Log.e("CMRERR", e.toString());
            }
        }
    }

    private class FaceDetectForProcess extends AsyncTask<Void, Void, Mat> {
        @Override
        protected Mat doInBackground(Void... params) {
            outputImg = FaceDetect();
            return outputImg;
        }

        @Override
        protected void onPostExecute(Mat result) {
            super.onPostExecute(result);
            try {
                Utils.matToBitmap(result, bitmap);
                iv_CameraView.setImageBitmap(bitmap);
            } catch (Exception e) {
                Log.e("CMRERR", e.toString());
            }
        }
    }

    private class FaceDetectForSetting extends AsyncTask<Void, Void, Mat> {
        @Override
        protected Mat doInBackground(Void... params) {
            outputImg = FaceDetect();
            return outputImg;
        }

        @Override
        protected void onPostExecute(Mat result) {
            super.onPostExecute(result);
            try {
                Utils.matToBitmap(result, bitmap);
                iv_CameraView.setImageBitmap(bitmap);
            } catch (Exception e) {
                Log.e("CMRERR", e.toString());
            }
        }
    }
}
