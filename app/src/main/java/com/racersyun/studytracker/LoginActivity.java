package com.racersyun.studytracker;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by racer on 2016-08-20.
 */
public class LoginActivity extends AppCompatActivity {
    private static final int DIALOG_PROGRESS_ID = 1;

    EditText et_UserIDEdit, et_UserPWEdit;
    TextView tv_LoginStatusView;
    Button btn_UserLoginBtn;
    CheckBox cb_AutoLoginEnableChkBox;

    boolean b_isLoginSuccess = false;
    boolean b_isBtnShowLogin = true;
    NetworkServiceClass nsc = new NetworkServiceClass();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle("계정 관리");
        setContentView(R.layout.activity_setting_login);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        et_UserIDEdit = (EditText) findViewById(R.id.et_UserIDEdit);
        et_UserPWEdit = (EditText) findViewById(R.id.et_UserPWEdit);
        tv_LoginStatusView = (TextView) findViewById(R.id.tv_LoginStatusView);
        cb_AutoLoginEnableChkBox = (CheckBox) findViewById(R.id.cb_AutoLoginEnableChkBox);
        btn_UserLoginBtn = (Button) findViewById(R.id.btn_UserLoginBtn);

        LoginStatusChk();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(this, MainActivity.class));
    }

    public void LoginStatusChk() {
        SharedPreferences prefs = getSharedPreferences("sp_LoginStatusCheck", MODE_PRIVATE);

        if (prefs.getBoolean("spref_isAlreadyLogin", false)) {
            setLoginedStatus(true);
            if (prefs.getBoolean("spref_isAutoLoginEnable", false)) {
                et_UserIDEdit.setText(prefs.getString("spref_AutoLoginUserID", ""));
                et_UserPWEdit.setText(prefs.getString("spref_AutoLoginUserPW", ""));
                cb_AutoLoginEnableChkBox.setChecked(true);
            } else {
                et_UserIDEdit.setText(prefs.getString("spref_ManualLoginUserID", ""));
                et_UserPWEdit.setText(prefs.getString("spref_ManualLoginUserPW", ""));
            }
        } else {
            setLoginedStatus(false);
        }
    }

    public void OnClick(View v) {
        switch (v.getId()) {
            case R.id.btn_UserRegisterBtn:
                startActivity(new Intent(LoginActivity.this, RegisterActivity.class));
                break;
            case R.id.btn_UserLoginBtn:
                if (b_isBtnShowLogin) {
                    showDialog(DIALOG_PROGRESS_ID);
                } else {
                    Logout();
                }
                break;
        }
    }

    protected void Login() {
        SharedPreferences prefs = getSharedPreferences("sp_LoginStatusCheck", MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();

        b_isLoginSuccess = nsc.b_SetDataForServer(nsc.SERVER_REQ_LOGIN, et_UserIDEdit.getText().toString(), et_UserPWEdit.getText().toString());
        if (b_isLoginSuccess) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    setLoginedStatus(true);
                }
            });

            if (cb_AutoLoginEnableChkBox.isChecked()) {
                editor.putString("spref_AutoLoginUserID", et_UserIDEdit.getText().toString());
                editor.putString("spref_AutoLoginUserPW", et_UserPWEdit.getText().toString());
                editor.putBoolean("spref_isAutoLoginEnable", cb_AutoLoginEnableChkBox.isChecked());
                editor.commit();
            }
            editor.putString("spref_ManualLoginUserID", et_UserIDEdit.getText().toString());
            editor.putString("spref_ManualLoginUserPW", et_UserPWEdit.getText().toString());
            editor.commit();
        } else {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    setLoginedStatus(false);
                    Toast toast_Msg = Toast.makeText(LoginActivity.this, "로그인 실패", Toast.LENGTH_SHORT);
                    toast_Msg.show();
                }
            });
        }
    }

    protected void Logout() {
        setLoginedStatus(false);
    }

    private void setUseableEditText(EditText et, boolean useable) {
        et.setClickable(useable);
        et.setEnabled(useable);
        et.setFocusable(useable);
        et.setFocusableInTouchMode(useable);
    }

    private void setUseableCheckBox(CheckBox cb, boolean useable) {
        cb.setClickable(useable);
        cb.setEnabled(useable);
        cb.setFocusable(useable);
        cb.setFocusableInTouchMode(useable);
    }

    private void setLoginedStatus(boolean useable) {
        SharedPreferences prefs = getSharedPreferences("sp_LoginStatusCheck", MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();

        if (useable) {
            b_isBtnShowLogin = false;

            setUseableEditText(et_UserIDEdit, false);
            setUseableEditText(et_UserPWEdit, false);
            setUseableCheckBox(cb_AutoLoginEnableChkBox, false);

            tv_LoginStatusView.setTextColor(Color.parseColor("#009900"));
            tv_LoginStatusView.setText("로그인 됨");
            btn_UserLoginBtn.setText("로그아웃");

            editor.putBoolean("spref_isAlreadyLogin", true);
            editor.commit();
        } else {
            b_isBtnShowLogin = true;

            setUseableEditText(et_UserIDEdit, true);
            setUseableEditText(et_UserPWEdit, true);
            setUseableCheckBox(cb_AutoLoginEnableChkBox, true);

            et_UserIDEdit.setText("");
            et_UserPWEdit.setText("");
            tv_LoginStatusView.setTextColor(Color.parseColor("#990000"));
            tv_LoginStatusView.setText("로그인 해 주세요");
            btn_UserLoginBtn.setText("로그인");

            editor.putString("spref_AutoLoginUserID", "");
            editor.putString("spref_AutoLoginUserPW", "");
            editor.putString("spref_ManualLoginUserID", "");
            editor.putString("spref_ManualLoginUserPW", "");
            editor.putBoolean("spref_isAutoLoginEnable", false);
            editor.putBoolean("spref_isAlreadyLogin", false);
            editor.commit();
        }
    }

    private class ProgressThread extends Thread {
        Handler mHandler;

        ProgressThread(Handler h) {
            mHandler = h;
        }

        public void run() {
            Login();
            Message msg = mHandler.obtainMessage();
            mHandler.sendMessage(msg);
        }
    }

    final Handler handler = new Handler() {
        public void handleMessage(Message msg) {
            dismissDialog(DIALOG_PROGRESS_ID);
            removeDialog(DIALOG_PROGRESS_ID);
        }
    };

    protected Dialog onCreateDialog(int id) {
        switch (id) {
            case DIALOG_PROGRESS_ID:
                ProgressDialog progressDialog = new ProgressDialog(LoginActivity.this);
                progressDialog.setMessage("처리중...");
                new ProgressThread(handler).start();
                return progressDialog;
        }
        return null;
    }
}
